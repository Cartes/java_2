import java.io.*;
public class FicheroEnBuffer {
	
	private String nombreFichero;
	
	public FicheroEnBuffer(String nombre)
	{
		nombreFichero = nombre;
	}
	
	public void EscribirTabla(String[] tabla)
	{
		try {
			
			
		File fichero = new File(nombreFichero);
		FileWriter fw = new FileWriter(fichero);
		BufferedWriter bw = new BufferedWriter(fw);
		
		for (int i=0; i<tabla.length; i++)
		{
			bw.write(tabla[i]);
			bw.newLine();
		}
		
		bw.close();
		
		}catch(IOException e) {System.out.println("ERROR");}
	}
	
	public void leerTabla(String[] tabla)
	{
		try {
			
			
		File fichero = new File(nombreFichero);
		FileReader fr = new FileReader(fichero);
		BufferedReader br = new BufferedReader(fr);
		String tmp;
		
		int i;
		i=0;
		tmp = br.readLine();

		
		while (tmp != null) {
			
			tabla[i] = tmp;
			i++;
			tmp= br.readLine();
		}
		br.close();
		
		}catch(IOException e) {System.out.println("ERROR");}
	}

}
