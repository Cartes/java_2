import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class FicheroAlumnos {
	
	private String nombreFichero;
	
	public FicheroAlumnos(String n)
	{
		nombreFichero = n;
	}
	
	public void EscribirTabla(Persona[] tabla)
	{
		try {
			
			
		File fichero = new File(nombreFichero);
		FileWriter fw = new FileWriter(fichero);
		BufferedWriter bw = new BufferedWriter(fw);
		
		for (int i=0; i<tabla.length; i++)
		{
			//Escribe una linea para cada data: nombre, edad
			bw.write(tabla[i].getNombre());
			bw.newLine();
			int numero = tabla[i].getEdad(); 
			String vNumero = Integer.toString(numero);
			bw.write(vNumero);
			bw.newLine();
			//Siempre que hacemos un campo es poner en la siguiente linea un new line para que no vaya todo junto
		}
		
		bw.close();
		
		}catch(IOException e) {System.out.println("ERROR");}
	}
	
	public void leerTabla(Persona[] tabla)
	{
		try {
			
			
		File fichero = new File(nombreFichero);
		FileReader fr = new FileReader(fichero);
		BufferedReader br = new BufferedReader(fr);
		String tmp;
		
		int i;
		i=0;
		tmp = br.readLine();
		String nombre;
		int edad;
		
		while (tmp != null) {
			/*
			//Lectura del nombre
			tabla[i].setNombre(tmp);
			tmp = br.readLine();
			tabla[i].setEdad(Integer.parseInt(tmp));
			i++;
			tmp= br.readLine();*/
			
			//Lectura con creaci�n de Persona(Con contructores)
			nombre = tmp;
			tmp = br.readLine();
			edad = Integer.parseInt(tmp);
			tabla[i] = new Persona(nombre,edad); //Creamos una nueva persona cada vez que lo llamamos
			i++;
			tmp = br.readLine();
		}
		br.close();
		
		}catch(IOException e) {System.out.println("ERROR");}

	}
	
	public void OrdenarEdad(Persona[] tabla)
	{
		
		try {
				
		File fichero = new File(nombreFichero);
		FileWriter fr = new FileWriter(fichero);
		BufferedWriter br = new BufferedWriter(fr);
		Persona personaMinAnt = new Persona(" ", -100);
		Persona personaMinAct = new Persona(" ", 100000);
		
		for (int i=0; i<tabla.length; i++)
		{	
			for(int j=0; j<tabla.length; j++)
			{
				if ((tabla[j].getEdad() > personaMinAnt.getEdad())/*Coge un minimo mas grande*/ && (tabla[j]).getEdad() < personaMinAct.getEdad())
				{
					personaMinAct = tabla[j];
				}
				
			}
			//Ya tenemos localizado el minimo y se escribe en el fichero
		}
		}catch(IOException e) {System.out.println("ERROR");}


	}
}
