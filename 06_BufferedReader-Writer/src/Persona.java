import java.io.*;
public class Persona {
	
	private String nombre;
	private int edad;
	
	public Persona()
	{
		nombre = "";
		edad= 0;
	}
	
	public Persona(String n, int e)
	{
		nombre = n;
		edad = e;
	}
	
	public void leertabla(String [] tabla)
	{
		for (int i=0; i<tabla.length; i++)
		System.out.println("El nombre y edad es : " + tabla[i]);
	}
	//Getters
	public String getNombre(){return nombre;}
	public int    getEdad()  {return edad;}
	
	//Setters
	public void setNombre(String n)
	{
		nombre = n;
	}
	
	public void setEdad(int x)
	{
		edad = x;
	}

}
