public class Cuadrado extends Figura{
	
	private double lado;

	public Cuadrado(String color, char tamano, double lado) {
		super(color, tamano);
		this.lado = lado;
	}

	public double getLado() {
		return lado;
	}

	public void setLado(double lado) {
		this.lado = lado;
	}

	
	@Override
	public String toString() {
		return "Cuadrado [color=" + color + ", tamano=" + tamano + ", lado=" + lado + "]";
	}

	public double calcularArea () {
		double area = getLado()*2;
		return area;

	}

}
