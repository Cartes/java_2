import java.util.*;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Principal {

	public static void main(String[] args) {
	int n=0;
	
	do {
			System.out.println("|----------------------------------------------|");
			System.out.println("|            Calculando AREAS                  |");
			System.out.println("|----------------------------------------------|");
			System.out.println("1. Circulo");
			System.out.println("2. Cuadrado");
			System.out.println("3. Triangulo");
			System.out.println("4. Rombo");
			System.out.println("5. Trapecio");
			System.out.println("6. Rectángulo");
			
			System.out.println("Escriba que opcion desea: ");
			
			Scanner teclado = new Scanner(System.in);
			try {
			n = teclado.nextInt();
			} catch (InputMismatchException error) {
				System.out.println("Introduzca una opcion correcta");
			}
			
			
		switch(n) {
		case 1:
			System.out.println("Esta calculando el area del Circulo: ");
			System.out.println("Radio: ");
			double radio = 0;
			try {
			radio = teclado.nextDouble();
			} catch (InputMismatchException error) {
				System.out.println("Introduzca un valor correcto para el lado");
			}
			Circulo cir = new Circulo("Gris", 'G', radio);
			System.out.println("El ares es: "+cir.calcularArea());
			break;
		/*
		case 2:
			System.out.println("Estas calculando el area del Cuadrado: ");
			System.out.println("Lado: ");
			double lado = teclado.nextDouble();
			Cuadrado cua = new Cuadrado("Rojo", 'P', lado);
			System.out.println("El area es: "+cua.calcularArea());
			break;
		
		case 3:
			System.out.println("Estas calculando el area de un Triangulo: ");
			System.out.println("Dime la base: ");
			double base = teclado.nextDouble();
			System.out.println("Dime la altura ");
			double altura = teclado.nextDouble();
			Triangulo tri = new Triangulo("Verde", 'G', base, altura);
			System.out.println("El area del Triangulo es: " + tri.calcularArea());
			break;
		/*
		case '4':
			System.out.println("Estas calculando el area de un Rombo");
			System.out.println("Dime la diagonal mayor: ");
			double dMayor = teclado.nextDouble();
			System.out.println("Dime la diagonal menor: ");
			double dMenor = teclado.nextDouble();
			Rombo rom = new Rombo("Verde", 'G', dMayor, dMenor);
			System.out.println("El area del Rombo es: " + rom.calcularArea(dMayor,dMenor));
			break;
			/*
		case '5':
			System.out.println("Estas calculando el area de un Trapecio");
			System.out.println("Dime la altura: ");
			double altura1= teclado.nextDouble();
			System.out.println("Dime la bMayor: ");
			double bMayor = teclado.nextDouble();
			System.out.println("Dime la bMenor: ");
			double bMenor = teclado.nextDouble();
			System.out.println("El area de un Trapecio es: " + Trapecio.calcularArea(altura, bMayor, bMenor));
			break;
		case '6':
			System.out.println("Estas calculando el area de un Rectangulo");
			System.out.println("Dime la base: ");
			double base1 = teclado.nextDouble();
			System.out.println("Dime la altura: ");
			double altura2 = teclado.nextDouble();
			System.out.println("El area de un Rectangulo es: " + Rectangulo.calcularArea(base, altura));
			break;
			*/
		default:
			System.out.println("Eleccion no valida");
		} 
	} while (n != 7);
}
}
