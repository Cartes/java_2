
public class Triangulo extends Figura{
	
	double base;
	double altura;
	public Triangulo(String color, char tamaño, double base, double altura) {
		super(color, tamaño);
		this.base = base;
		this.altura = altura;
	}
	public double getBase() {
		return base;
	}
	public void setBase(double base) {
		this.base = base;
	}
	public double getAltura() {
		return altura;
	}
	public void setAltura(double altura) {
		this.altura = altura;
	}
	@Override
	public String toString() {
		return "Triangulo [color=" + color + ", tamaño=" + tamaño + ", base=" + base + ", altura=" + altura + "]";
	}
	
	public double calcularArea () {
		double retornarArea;
		retornarArea = (getBase()*getAltura())/2;
		return retornarArea;
	}

}
