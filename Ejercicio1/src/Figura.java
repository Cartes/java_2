
public class Figura {
	
	protected String color;
	protected char tamano;
	
	/*TIPOS DE VISIBILIDAD EN JAVA*/
	//Public --> Visibles para todas las demas clases
	//Protected --> Se utiliza en herencia, los llevan las clases PADRE y solo es visible para las clases HIJAS. Excepto los metodos Constructor y toString.
	//Private --> Se utiliza en las clases HIJAS para los atributos, Mantener los GET Y SET publicos
	
	public Figura(String color, char tamano) {
		super();
		this.color = color;
		this.tamano = tamano;
	}


	protected String getColor() {
		return color;
	}


	protected void setColor(String color) {
		this.color = color;
	}


	protected char getTamano() {
		return tamano;
	}


	protected void setTamano(char tamano) {
		this.tamano = tamano;
	}


	@Override
	public String toString() {
		return "Figura [color=" + color + ", tamano=" + tamano + "]";
	}
	
	
	
	

}
