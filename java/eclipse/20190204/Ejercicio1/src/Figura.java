
public class Figura {
	
	protected String color;
	protected char tamaño;
	
	/*TIPOS DE VISIBILIDAD EN JAVA*/
	//Public --> Visibles para todas las demas clases
	//Protected --> Se utiliza en herencia, los llevan las clases PADRE y solo es visible para las clases HIJAS. Excepto los metodos Constructor y toString.
	//Private --> Se utiliza en las clases HIJAS para los atributos, Mantener los GET Y SET publicos
	
	public Figura(String color, char tamaño) {
		super();
		this.color = color;
		this.tamaño = tamaño;
	}


	protected String getColor() {
		return color;
	}


	protected void setColor(String color) {
		this.color = color;
	}


	protected char getTamaño() {
		return tamaño;
	}


	protected void setTamaño(char tamaño) {
		this.tamaño = tamaño;
	}


	@Override
	public String toString() {
		return "Figura [color=" + color + ", tamaño=" + tamaño + "]";
	}
	
	
	
	

}
