
public class Rombo extends Figura{
	
	private double dMayor;
	private double dMenor;
	
	
	public Rombo(String color, char tamaño, double dMayor, double dMenor) {
		super(color, tamaño);
		this.dMayor = dMayor;
		this.dMenor = dMenor;
	}
	
	
	public double getdMayor() {
		return dMayor;
	}
	public void setdMayor(double dMayor) {
		this.dMayor = dMayor;
	}
	public double getdMenor() {
		return dMenor;
	}
	public void setdMenor(double dMenor) {
		this.dMenor = dMenor;
	}


	@Override
	public String toString() {
		return "Rombo [color=" + color + ", tamaño=" + tamaño + ", dMayor=" + dMayor + ", dMenor=" + dMenor + "]";
	}
	
	public double calcularArea() {

		return (getdMayor*getdMenor)/2;
	}

}
