public class Trapecio extends Figura{
	
	double bMayor;
	double bMenor;
	double altura;
	
	
	public Trapecio(String color, char tamaño, double bMayor, double bMenor, double altura) {
		super(color, tamaño);
		this.bMayor = bMayor;
		this.bMenor = bMenor;
		this.altura = altura;
	}


	public double getbMayor() {
		return bMayor;
	}


	public void setbMayor(double bMayor) {
		this.bMayor = bMayor;
	}


	public double getbMenor() {
		return bMenor;
	}


	public void setbMenor(double bMenor) {
		this.bMenor = bMenor;
	}


	public double getAltura() {
		return altura;
	}


	public void setAltura(double altura) {
		this.altura = altura;
	}


	@Override
	public String toString() {
		return "Trapecio [color=" + color + ", tamaño=" + tamaño + ", bMayor=" + bMayor + ", bMenor=" + bMenor
				+ ", altura=" + altura + "]";
	}
	
	
	public static double calcularArea (double altura, double bMayor, double bMenor) { 
		/* double par;
		 * par = ((getbMayor()+getbMenor())/2)*getaltura();
		 * return par;
		 */
		double calcular = altura * (bMayor+bMenor) / 2;
		return calcular;
	}

}
