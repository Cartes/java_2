import java.util.*;

public class Principal2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		/*
		 * //Sacar por pantalla los primeros 10 numeros enteros int n = 0;
		 * 
		 * while (n<=10) { System.out.println(n); n++; }
		 */
		// Sacar por pantalla los primeros 10 numeros enteros pares
		/*
		 * int n = 0;
		 * 
		 * while (n <= 20) { System.out.println(n); n=n+2;
		 */
		// de entre los 20 primeros numeros, aquelslos múltiplos de 7 ó de 3
		int n = 1;

		while (n <= 20) {
			if ((n % 3 == 0) || (n % 7 == 0)) {
				System.out.println(n);
			}
			n++;
		}
		System.out.println("Menu calculadora");
		// si elijo + sumar
		// si elijo - restar
		// si elijo * multiplicar
		// si elijo / dividir
		// si elijo @ salir
		
		System.out.println("Elija opcion: ");
		Scanner teclado = new Scanner(System.in); //Objeto en Java // Scanner es una cosa que permite meter cosas por teclado.
		char seleccion = teclado.next().charAt(0); //Charat0 la primera letra que se coga es va a asignar a la variable "seleccion" coge la primera.

		switch (seleccion) {
		case '+':
			System.out.println("Sumar");
			break;
		case '-':
			System.out.println("Restar");
			break;
		case '*':
			System.out.println("Multiplicar");
			break;
		case '/':
			System.out.println("Dividir");
			break;
		case '@':
			System.out.println("Hasta luego");
			break;
		default:
			System.out.println("Eleccion no valida");
			break;
		}
	}

}
