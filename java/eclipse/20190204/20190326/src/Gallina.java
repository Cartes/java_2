
public class Gallina extends Animal{
	
	//Atributos de la clase Gallina
	int numHuevos;
	boolean tieneCresta;
	
	//Metodo Constructor
	public Gallina(String nombre, int edad, boolean chip, double peso, String color, int numHuevos,
			boolean tieneCresta) {
		super(nombre, edad, chip, peso, color);
		this.numHuevos = numHuevos;
		this.tieneCresta = tieneCresta;
	}

	
	//Metodo get & set
	public int getNumHuevos() {
		return numHuevos;
	}

	public void setNumHuevos(int numHuevos) {
		this.numHuevos = numHuevos;
	}

	public boolean isTieneCresta() {
		return tieneCresta;
	}

	public void setTieneCresta(boolean tieneCresta) {
		this.tieneCresta = tieneCresta;
	}

	
	//Metodo toString

	@Override
	public String toString() {
		return "Gallina [nombre=" + nombre + ", edad=" + edad + ", chip=" + chip + ", peso=" + peso + ", color=" + color
				+ ", numHuevos=" + numHuevos + ", tieneCresta=" + tieneCresta + "]";
	}
	
	
	
	
	
	

}
