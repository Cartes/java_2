
public class Conejo extends Animal{
	
	//Atributos de la clase Conejo
	int tamaño;
	boolean pelo;
	boolean campero;
	
	//Metodo Constructor
	public Conejo(String nombre, int edad, boolean chip, double peso, String color, int tamaño, boolean pelo,
			boolean campero) {
		super(nombre, edad, chip, peso, color);
		this.tamaño = tamaño;
		this.pelo = pelo;
		this.campero = campero;
	}

	
	//Metodos get & set
	public int getTamaño() {
		return tamaño;
	}

	public void setTamaño(int tamaño) {
		this.tamaño = tamaño;
	}

	public boolean isPelo() {
		return pelo;
	}

	public void setPelo(boolean pelo) {
		this.pelo = pelo;
	}

	public boolean isCampero() {
		return campero;
	}

	public void setCampero(boolean campero) {
		this.campero = campero;
	}

	
	//Metodo toString
	@Override
	public String toString() {
		return "Conejo [nombre=" + nombre + ", edad=" + edad + ", chip=" + chip + ", peso=" + peso + ", color=" + color
				+ ", tamaño=" + tamaño + ", pelo=" + pelo + ", campero=" + campero + "]";
	}
	
	
	

}
