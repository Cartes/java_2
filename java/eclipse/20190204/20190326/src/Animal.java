
public class Animal {
	
	//Atributos de la clase Animal(PADRE)
	String nombre;
	int edad;
	boolean chip;
	double peso;
	String color;
	
	//Metodo constructor
	public Animal(String nombre, int edad, boolean chip, double peso, String color) {
		super();
		this.nombre = nombre;
		this.edad = edad;
		this.chip = chip;
		this.peso = peso;
		this.color = color;
	}
	
	//Metodos get & set

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	public boolean isChip() {
		return chip;
	}

	public void setChip(boolean chip) {
		this.chip = chip;
	}

	public double getPeso() {
		return peso;
	}

	public void setPeso(double peso) {
		this.peso = peso;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	
	//Metodo toString
	@Override
	public String toString() {
		return "Animal [nombre=" + nombre + ", edad=" + edad + ", chip=" + chip + ", peso=" + peso + ", color=" + color
				+ "]";
	}
	
	
	
	
	

}
