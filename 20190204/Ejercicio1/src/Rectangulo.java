
public class Rectangulo extends Figura{
	
	double base;
	double altura;
	
	
	public Rectangulo(String color, char tamaño, double base, double altura) {
		super(color, tamaño);
		this.base = base;
		this.altura = altura;
	}


	public double getBase() {
		return base;
	}


	public void setBase(double base) {
		this.base = base;
	}


	public double getAltura() {
		return altura;
	}


	public void setAltura(double altura) {
		this.altura = altura;
	}


	@Override
	public String toString() {
		return "Rectangulo [color=" + color + ", tamaño=" + tamaño + ", base=" + base + ", altura=" + altura + "]";
	}
	
	
	public static double calcularArea(double base, double altura) {
		/*
		 * double a;
		 * a = getbase() * getaltura();
		 */
		double calcular = base * altura;
		return calcular;
	}
	
	

}
