import java.net.*;
import java.io.*;

public class Traductor {
	
	public static void main(String[] args) throws Exception {
		
		String palabra = "mesa";
		
		URL direccion = new URL("https://dictionary.cambridge.org/es/diccionario/espanol-ingles/"+palabra);
		String html = obtenerHTML (direccion); // Se coge todo el HTML 
		String palabraTraducida = cortarURL(html, palabra); // Coge todo el tocho de la URL y la palabra traducida
		System.out.println(palabraTraducida);
		//System.out.println(html);
	}
	
	public static String obtenerHTML (URL direccionWeb) throws Exception { // Se le pasa el parametro de la URL.
		
		BufferedReader in = new BufferedReader(new InputStreamReader(direccionWeb.openStream()));
		String inputLine, codigo="";

		while ((inputLine = in.readLine()) != null)
			codigo+=inputLine;

		in.close();
		
		return codigo;
	}
	
	public static String cortarURL (String html, String palabra) {
		int ini = html.indexOf("traducir "+palabra+": ")+11+palabra.length(); //EL +11 es por la palabra traducir el espacio y los 2 puntos + el espacio, para que corte la palabra.
		int fin = html.indexOf(". ");
		String cortar = html.substring(ini, fin);
		return cortar;
	}
}