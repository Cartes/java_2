import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.FlowLayout;
import java.awt.Color;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.ImageIcon;
import javax.swing.JCheckBox;
import java.awt.Cursor;
import java.awt.Font;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JTextArea;
import javax.swing.JScrollPane;
import javax.swing.JPasswordField;
import javax.swing.DropMode;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.AbstractListModel;
import javax.swing.ListSelectionModel;
import java.awt.Dialog.ModalExclusionType;
import java.awt.Toolkit;
import com.toedter.calendar.JCalendar;
import javax.swing.border.CompoundBorder;
import com.toedter.calendar.JDateChooser;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;

public class GUI {

	//Un Jframe representa la ventana en su totalidad
	private JFrame frmEncuesta; 
	private JTextField usuario;
	private final ButtonGroup buttonGroup = new ButtonGroup(); // Con esto crea los grupos de los radioButton
	private final ButtonGroup buttonGroup_1 = new ButtonGroup();
	private JList list;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUI window = new GUI();
					window.frmEncuesta.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public GUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmEncuesta = new JFrame();
		frmEncuesta.setIconImage(Toolkit.getDefaultToolkit().getImage("C:\\Users\\prueba\\Pictures\\Saved Pictures\\wow.jpg"));
		frmEncuesta.setTitle("Encuesta");
		frmEncuesta.setBounds(100, 100, 823, 661); //Cambiamos la posicion y el tama�o X | Y | Ancho | Largo
		frmEncuesta.setLocationRelativeTo(null); //Sirve para centrar el contenido
		frmEncuesta.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmEncuesta.getContentPane().setLayout(null);
		
		JButton aceptar = new JButton("Aceptar");
		aceptar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		aceptar.setBounds(323, 205, 89, 23);
		frmEncuesta.getContentPane().add(aceptar);
		
		usuario = new JTextField();
		usuario.setToolTipText("Introduzca su Nombre");
		usuario.setBounds(234, 135, 178, 23);
		frmEncuesta.getContentPane().add(usuario);
		usuario.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("Introduzca Usuario:");
		lblNewLabel.setHorizontalAlignment(SwingConstants.TRAILING);
		lblNewLabel.setBounds(92, 139, 138, 14);
		frmEncuesta.getContentPane().add(lblNewLabel);
		
		JLabel foto = new JLabel("Dia a recordar");
		foto.setFont(new Font("Arial", Font.PLAIN, 13));
		foto.setIcon(new ImageIcon("C:\\Users\\CFGS\\Pictures\\Saved Pictures\\descarga.jpg"));
		foto.setBounds(0, 463, 412, 169);
		frmEncuesta.getContentPane().add(foto);
		
		JCheckBox check = new JCheckBox("Eres del bar\u00E7a");
		check.setHorizontalAlignment(SwingConstants.LEFT);
		check.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		check.setFont(new Font("Times New Roman", Font.PLAIN, 13));
		check.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		check.setBounds(166, 205, 141, 23);
		frmEncuesta.getContentPane().add(check);
		
		JRadioButton rdbtnNewRadioButton = new JRadioButton("Alumno");
		rdbtnNewRadioButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		buttonGroup_1.add(rdbtnNewRadioButton);
		rdbtnNewRadioButton.setBounds(139, 347, 109, 23);
		frmEncuesta.getContentPane().add(rdbtnNewRadioButton);
		
		JRadioButton Profesor = new JRadioButton("Profesor");
		buttonGroup_1.add(Profesor);
		Profesor.setBounds(139, 373, 109, 23);
		frmEncuesta.getContentPane().add(Profesor);
		
		JRadioButton rdbtnNewRadioButton_2 = new JRadioButton("Hombre");
		buttonGroup.add(rdbtnNewRadioButton_2);
		rdbtnNewRadioButton_2.setBounds(24, 347, 109, 23);
		frmEncuesta.getContentPane().add(rdbtnNewRadioButton_2);
		
		JRadioButton Mujer = new JRadioButton("Mujer");
		buttonGroup.add(Mujer);
		Mujer.setBounds(24, 373, 109, 23);
		frmEncuesta.getContentPane().add(Mujer);
		
		JLabel lblNewLabel_2 = new JLabel("Seleccione ciudad:");
		lblNewLabel_2.setBounds(24, 403, 122, 23);
		frmEncuesta.getContentPane().add(lblNewLabel_2);
		
		JComboBox comboBox = new JComboBox();

		comboBox.setMaximumRowCount(4);
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"MADRID", "BARCELONA", "VALENCIA", "SEVILLA", "BILBAO", "ZARAGOZA", "ALBACETE"}));
		comboBox.setSelectedIndex(0);
		comboBox.setBounds(137, 404, 103, 20);
		frmEncuesta.getContentPane().add(comboBox);
		
		JLabel lblObservaciones = new JLabel("Observaciones:");
		lblObservaciones.setBounds(591, 429, 109, 23);
		frmEncuesta.getContentPane().add(lblObservaciones);
		
		JPasswordField Acepto = new JPasswordField();
		Acepto.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
			}
		});
		Acepto.setEnabled(false);
		Acepto.setCursor(Cursor.getPredefinedCursor(Cursor.NW_RESIZE_CURSOR));
		Acepto.setToolTipText("Introduzca Password");
		Acepto.setEchoChar('*');
		Acepto.setBounds(234, 160, 178, 23);
		frmEncuesta.getContentPane().add(Acepto);
		
		JLabel lblIntroduzcaConstrasea = new JLabel("Constrase\u00F1a:");
		lblIntroduzcaConstrasea.setBounds(153, 164, 78, 14);
		frmEncuesta.getContentPane().add(lblIntroduzcaConstrasea);
		
		JLabel lblAficiones = new JLabel("Aficiones");
		lblAficiones.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblAficiones.setBounds(611, 257, 102, 23);
		frmEncuesta.getContentPane().add(lblAficiones);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(611, 283, 141, 117);
		frmEncuesta.getContentPane().add(scrollPane_1);
		
		list = new JList();
		list.setVisibleRowCount(2);
		scrollPane_1.setViewportView(list);
		list.setModel(new AbstractListModel() {
			String[] values = new String[] {"SALIR DE FIESTA", "VIAJES" , "SENDERISMO", "HACER DEPORTE", "PROGRAMAR JAVA", "LEER LIBROS", "CORRER", "IR AL GYM"};
			public int getSize() {
				return values.length;
			}
			public Object getElementAt(int index) {
				return values[index];
			}
		});
		list.setSelectedIndex(2);
		list.setToolTipText("Seleccione una aficcion");
		
		JTextArea textArea = new JTextArea();
		textArea.setBounds(591, 453, 216, 167);
		frmEncuesta.getContentPane().add(textArea);
		textArea.setLineWrap(true);
		
		JCalendar calendar = new JCalendar();
		calendar.getDayChooser().setBorder(new CompoundBorder());
		calendar.getDayChooser().getDayPanel().setBorder(new CompoundBorder());
		calendar.setBounds(591, 24, 184, 153);
		frmEncuesta.getContentPane().add(calendar);
		
		JDateChooser dateChooser = new JDateChooser();
		dateChooser.setDateFormatString("dd-MM-yy");
		dateChooser.setBounds(342, 24, 95, 20);
		frmEncuesta.getContentPane().add(dateChooser);
		
		JLabel lblElijaFecha = new JLabel("Elija fecha:");
		lblElijaFecha.setBounds(278, 24, 62, 20);
		frmEncuesta.getContentPane().add(lblElijaFecha);
		
		//Eventos
		
		usuario.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				Acepto.setEnabled(true);
	
			}	
		});
		frmEncuesta.addWindowListener(new WindowAdapter() {
			@Override
			public void windowOpened(WindowEvent arg0) {
				Profesor.setSelected(true);
				Mujer.setSelected(true);
				
			}
		});
		
		rdbtnNewRadioButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int respuesta = JOptionPane.showConfirmDialog(frmEncuesta,
						"�Realmente eres un Alumno?", "Confirmar por favor",
						JOptionPane.YES_NO_OPTION,
						JOptionPane.QUESTION_MESSAGE);
				if (respuesta == 0) {
					textArea.setText("Eres Alumno");
				}
				if (respuesta == 1) {
					textArea.setText("No eres Alumno");
				}
			}
		});
		/*
		check.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(check.isSelected()) {
				aceptar.setEnabled(true);
				}
				else {
				aceptar.setEnabled(false);
				}
			}
		});
		*/
		comboBox.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				//Cuando elijo Barcelona, c	ambiar imagen
				if (comboBox.getSelectedIndex()==1) {
					foto.setIcon(new ImageIcon("C:\\Users\\prueba\\Desktop\\pique.jpeg"));
					
				}
			}
		});
		
		aceptar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//Si hay usuario y password mensaje OK
				if (!usuario.getText().isEmpty() && !Acepto.getText().isEmpty()) {
					JOptionPane.showMessageDialog(null, "Bienvenido "+usuario.getText());
				}
				//Si no --> Mensaje no OK
				else {
					JOptionPane.showMessageDialog(null, "Faltan datos");
				}
				

			}
		});
	}
	
}
