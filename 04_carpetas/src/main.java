import java.io.File;

public class main {

	public static void main(String[] args) {
		
		
		String[] rutas = {
				"Proyecto1",
				"Proyecto1\\bin",
				"Proyecto1\\bin\\bytecode",
				"Proyecto1\\bin\\objetos",
				"Proyecto1\\src",
				"Proyecto1\\src\\clases",
				"Proyecto1\\doc",
				"Proyecto1\\doc\\html",
				"Proyecto1\\doc\\pdf",
				
		};
		
		boolean seguir = true;
		
		for (int i=0; (i<rutas.length) && (seguir==true); i++) 
		{
			File carpeta = new File(rutas[i]);
			if (carpeta.mkdir() == false) 
				seguir = false;
			else 
				seguir = true;
		}
		System.out.println("Carpetas Creadas");

	}

}
