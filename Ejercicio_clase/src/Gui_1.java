import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JProgressBar;
import com.toedter.calendar.JDateChooser;
import java.awt.Panel;
import javax.swing.JToggleButton;
import javax.swing.JTree;
import javax.swing.JSpinner;
import javax.swing.JSeparator;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JTextPane;
import javax.swing.JScrollBar;
import java.awt.SystemColor;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Gui_1 {

	private JFrame frame;
	private JTextField Nombre1;
	private JTextField Caja_apellido;
	private JTextField caja_apellido2;
	private final ButtonGroup buttonGroup = new ButtonGroup();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Gui_1 window = new Gui_1();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Gui_1() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setForeground(SystemColor.desktop);
		frame.setBounds(100, 100, 698, 776);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblFormulario = new JLabel("PROGRAMA M\u00C9DICO DE PATOLOGIAS CRONICAS");
		lblFormulario.setFont(new Font("Tekton Pro Ext", Font.PLAIN, 14));
		lblFormulario.setBounds(10, 11, 385, 37);
		frame.getContentPane().add(lblFormulario);
		
		Nombre1 = new JTextField();

		Nombre1.setBounds(105, 59, 104, 20);
		frame.getContentPane().add(Nombre1);
		Nombre1.setColumns(10);
		
		JLabel Nombre = new JLabel("Nombre cliente:");
		Nombre.setBounds(10, 62, 85, 14);
		frame.getContentPane().add(Nombre);
		
		Caja_apellido = new JTextField();

		Caja_apellido.setEnabled(false);

		Caja_apellido.setBounds(278, 59, 86, 20);
		frame.getContentPane().add(Caja_apellido);
		Caja_apellido.setColumns(10);
		
		JLabel Apellido_1 = new JLabel("Apellido 1");
		Apellido_1.setBounds(214, 62, 66, 14);
		frame.getContentPane().add(Apellido_1);
		
		caja_apellido2 = new JTextField();
		caja_apellido2.setEnabled(false);
		caja_apellido2.setColumns(10);
		caja_apellido2.setBounds(438, 59, 86, 20);
		frame.getContentPane().add(caja_apellido2);
		
		JLabel Apellido_2 = new JLabel("Apellido 2");
		Apellido_2.setBounds(374, 62, 57, 14);
		frame.getContentPane().add(Apellido_2);
		
		JLabel lblFechaNacimiento = new JLabel("F.Nacimiento:");
		lblFechaNacimiento.setBounds(10, 124, 91, 14);
		frame.getContentPane().add(lblFechaNacimiento);
		
		JDateChooser dateChooser = new JDateChooser();
		dateChooser.setBounds(105, 118, 104, 20);
		frame.getContentPane().add(dateChooser);
		
		JLabel lblSexo = new JLabel("Sexo");
		lblSexo.setBounds(226, 124, 66, 14);
		frame.getContentPane().add(lblSexo);
		
		JRadioButton rdbtnHombre = new JRadioButton("Hombre");

		buttonGroup.add(rdbtnHombre);
		rdbtnHombre.setBounds(265, 104, 99, 23);
		frame.getContentPane().add(rdbtnHombre);
		
		JRadioButton rdbtnMujer = new JRadioButton("Mujer");

		buttonGroup.add(rdbtnMujer);
		rdbtnMujer.setBounds(265, 136, 99, 23);
		frame.getContentPane().add(rdbtnMujer);
		
		JLabel lblDiagnostico = new JLabel("DIAGNOSTICO(marque las que cree que tiene)");
		lblDiagnostico.setFont(new Font("Tekton Pro Ext", Font.PLAIN, 14));
		lblDiagnostico.setBounds(10, 248, 385, 37);
		frame.getContentPane().add(lblDiagnostico);
		
		JCheckBox chckbxNewCheckBox = new JCheckBox("Arritmia");
		chckbxNewCheckBox.setBounds(10, 300, 97, 23);
		frame.getContentPane().add(chckbxNewCheckBox);
		
		JCheckBox chckbxDiabetesTipoI = new JCheckBox("Diabetes Tipo I");
		chckbxDiabetesTipoI.setBounds(112, 300, 97, 23);
		frame.getContentPane().add(chckbxDiabetesTipoI);
		
		JCheckBox chckbxAsma = new JCheckBox("Asma");
		chckbxAsma.setBounds(226, 300, 97, 23);
		frame.getContentPane().add(chckbxAsma);
		
		JCheckBox chckbxAntic = new JCheckBox("Anticoagulaci\u00F3n");
		chckbxAntic.setBounds(325, 300, 110, 23);
		frame.getContentPane().add(chckbxAntic);
		
		JCheckBox chckbxInsuficienteCoronaria = new JCheckBox("EPOC");
		chckbxInsuficienteCoronaria.setBounds(10, 335, 97, 23);
		frame.getContentPane().add(chckbxInsuficienteCoronaria);
		
		JCheckBox chckbxDiabetesTipoIi = new JCheckBox("Diabetes Tipo II");
		chckbxDiabetesTipoIi.setBounds(112, 335, 104, 23);
		frame.getContentPane().add(chckbxDiabetesTipoIi);
		
		JCheckBox chckbxParkinson = new JCheckBox("Parkinson");
		chckbxParkinson.setBounds(226, 335, 97, 23);
		frame.getContentPane().add(chckbxParkinson);
		
		JCheckBox chckbxGota = new JCheckBox("Epilepsia");
		chckbxGota.setBounds(325, 335, 110, 23);
		frame.getContentPane().add(chckbxGota);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"Si", "De vez encuando", "Muchas veces", "Alguna que otra vez", "Ninguna vez antes", "Nunca"}));
		comboBox.setBounds(10, 423, 164, 20);
		frame.getContentPane().add(comboBox);
		
		JLabel lblleHaPasado = new JLabel("\u00BFLe habia pasado esto antes?");
		lblleHaPasado.setBounds(10, 398, 164, 14);
		frame.getContentPane().add(lblleHaPasado);
		
		JProgressBar BarraProgreso = new JProgressBar();

		BarraProgreso.setIndeterminate(true);
		BarraProgreso.setBounds(214, 668, 110, 14);
		frame.getContentPane().add(BarraProgreso);
		
		JTextPane textPane = new JTextPane();
		textPane.setBounds(10, 504, 321, 88);
		frame.getContentPane().add(textPane);
		
		JLabel lblOtroTipoDe = new JLabel("Otro tipo de Sintomas:");
		lblOtroTipoDe.setBounds(10, 476, 126, 14);
		frame.getContentPane().add(lblOtroTipoDe);
		
		JCheckBox Acepto = new JCheckBox("Aceptos las condiciones");

		Acepto.setBounds(191, 638, 164, 23);
		frame.getContentPane().add(Acepto);
		
		/*Eventos*/
		rdbtnHombre.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int respuesta = JOptionPane.showConfirmDialog(rdbtnHombre,
						"De verdad eres hombre?", "Confirmar por favor",
						JOptionPane.YES_NO_OPTION,
						JOptionPane.QUESTION_MESSAGE);
				JLabel textArea = null;
				if (respuesta == 0) {
					textArea.setText("Eres Alumno");
				}
				if (respuesta == 1) {
					textArea.setText("No eres Alumno");
				}
			}
		});
		
		Acepto.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int respuesta = JOptionPane.showConfirmDialog(Acepto,
						"Seguro has terminado?", "Confirmar por favor",
						JOptionPane.YES_NO_OPTION,
						JOptionPane.QUESTION_MESSAGE);
				JLabel textArea = null;
				if (respuesta == 0) {
					textArea.setText("Eres Alumno");
				}
				if (respuesta == 1) {
					textArea.setText("No eres Alumno");
				}
			}
		});
		
		
		Nombre1.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				Caja_apellido.setEnabled(true);
			}
		});
		
		Caja_apellido.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				caja_apellido2.setEnabled(true);
			}
		});

		
	}
}
