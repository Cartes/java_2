fun max(vararg numbers: Int) =
	numbers.reduce {max, e -> if(max>e) max else e }

	val values = intArrayOf(4, 5, 15, 2)


fun fors(){
	for (x in 1..10){ //Del 1 al 10
		println(x);
	}
	println("_")
	for (x in 1 until 10){ // Del 1 al 9
		println(x);
	}
	println("_");
	for (x in 10 downTo 1 step 2){ // Empezamos en el 10 y saltamos de 2 en 2
		println(x);
	}
}

fun main(){
			println(max(1, 2))
	
			println(max(7, 3, 9, 4))
			
			println(max(1, 2, 3, *values, 9, 2))
	
	
			fors();

		}