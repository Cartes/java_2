fun for_names(){
	val names = listOf("Alvaro", "Javier", "Alejandro");
	
	for (name in names){ //Saca el numero de veces, tanto como el numero de nombres
		println(names);
	}
	
	for (index in names.indices){ //Cuenta cuantos nombres hay en la lista indicada
		println(index);
	}
	
	for (index in names.indices){
		println("$index ${names.get(index)}"); // Indica el numero de cada elementos de la lista
	}
}

fun main(){
	for_names();
}