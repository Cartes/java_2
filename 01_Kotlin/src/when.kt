fun process(input: Any){
	when (input) { //Se va a una de la opciones "Cuando" es una de las indicadas
		1 -> println("Hola");
		7,8 -> println("estas en la lista de 7 y 8");
		in 12..19 -> println("estas dentro de la lista de 12 a 19");
		is String -> println("Hola ${input.length}"); //Longitud de lo indicado en el processo
		else -> println("Distinto");
	}
	

}

fun main(){
	process(1);
	process(7);
	process(8);
	process(16);
	process("Hola");
	process(StringBuilder());
}