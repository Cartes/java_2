fun main (args: Array<String>){
	
	val name = "Alejandro"
	val msg = "Hola gente! $name";
	val numero = 4.5712312312132132;
	println(msg.trimMargin("-"));	
	println(numero);
	
	
	val age = 11;
	val canVote = if (age> 17) "tienes la edad" else "no tienes la edad"
	println(canVote);
	
	greet("adios", "Alejandro");
	greet("Cartes");
	greet(msg="Hola de nuevo", name="Cartex");
	greet("Alvaro", msg="Hola!");
	
}

fun greet (name: String, msg: String = "Hi ${name.length}") {
	println("$msg $name");
}


