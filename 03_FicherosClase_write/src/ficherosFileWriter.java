import java.io.*;
public class ficherosFileWriter {
	
	
	public static void leerFichero(String nombre, String [] nombres) {
			
		try {
			//1.Apertura del Fichero
			File fichero = new File(nombre);
			FileReader fr= new FileReader(fichero);
			
			//2.Lectura del Fichero
			int letra;
			String cadena = "";
			int posicion = 0;
			
			letra = fr.read();
			while (letra != -1) {
				
				if (letra==';') {
					nombres[posicion] = cadena;
					cadena="";
					posicion++;
				}
				else
				cadena += ((char)letra); //pone los caracteres
				
				//Leer el siguiente caracter
				letra = fr.read();
			}
			//3.Cerrar el fichero
			fr.close();
			
			//4.Escribir la cadena por pantalla
			
			System.out.println("He leido: "+cadena);
		} catch (FileNotFoundException e) {System.out.println(e.getMessage());} catch (IOException e){System.out.println(e.getMessage());}
	}
		
	public static void escribirFichero(String nombre, String [] nombres) {
		
		try {
			//1. Apertura del fichero
			File fichero = new File(nombre);
			FileWriter fw = new FileWriter(fichero);
			//2.Escribir en el fichero
			for (int i=0; i<nombres.length; i++) {
			fw.write(nombres[i] + ";");
			}
			
			//3.Cerrar el fichero
			fw.close();
			
			
		} catch (IOException e) {System.out.println(e.getMessage());}
		
	}
	
	public static void InicializarTabla(String[] nombres) {
		for (int i=0; i<nombres.length; i++) {
			nombres[i]="";
		}
	}
	
	public static void EscribirTabla(String[] nombres) {
		System.out.println("Contenido de la tabla");
		for (int i=0; i<nombres.length; i++) {
			System.out.print(nombres[i] + " ");
		}
	}
	

	
	public static void LeerSalto(String nombre, String [] nombres) {
		
		try {
			//1.Apertura del Fichero
			File fichero = new File(nombre);
			FileReader fr= new FileReader(fichero);
			
			//2.Lectura del Fichero
			int letra;
			String cadena = "";
			int posicion = 0;
			
			letra = fr.read();
			while (letra != -1) {
				
				if (letra=='\n') {
					nombres[posicion] = cadena;
					cadena="";
					posicion++;
				}
				else
				cadena += ((char)letra); //pone los caracteres
				
				//Leer el siguiente caracter
				letra = fr.read();
			}
			//3.Cerrar el fichero
			fr.close();
			
			//4.Escribir la cadena por pantalla
			
			System.out.println("He leido: "+cadena);
		} catch (FileNotFoundException e) {System.out.println(e.getMessage());} catch (IOException e){System.out.println(e.getMessage());}
	}
	
	public static void EscribirSalto(String nombre, String[] nombres) {
		
		try {
			//1. Apertura del fichero
			File fichero = new File(nombre);
			FileWriter fw = new FileWriter(fichero);
			//2.Escribir en el fichero
			for (int i=0; i<nombres.length; i++) {	
			fw.write(nombres[i] + " ");
			}
			
			//3.Cerrar el fichero
			fw.close();
			
			
		} catch (IOException e) {System.out.println(e.getMessage());}
		
	}
	
	public static void InicializarTablaSalto(String[] nombres) {
		for (int i=0; i<nombres.length; i++) {
			nombres[i]="";
		}
	}
	
	public static void EscribirTablaSalto(String[] nombres) {
		System.out.println("Contenido de la tabla: ");
		for (int i=0; i<nombres.length; i++) {
			System.out.print(nombres[i] + "\n");
		}
	}

	public static void main(String[] args) {
		File fichero = new File("FicheroTexto1.txt");
		
		String [] listaNombres = { "Pepe", "Laura", "Juan", "Ana"};
		
	
		//Llamada escribir ficheross
			escribirFichero("FicheroTexto1.txt", listaNombres);
			
			InicializarTabla(listaNombres);
			EscribirTabla(listaNombres);
			
		//Llamada a leer el fichero
			leerFichero("FicheroTexto1.txt",listaNombres);
			EscribirTabla(listaNombres);
			
			System.out.println("\r\r");
			
			
			EscribirSalto("FicheroTexto1.txt", listaNombres);
			
			
			LeerSalto("FicheroTexto1.txt",listaNombres);
			EscribirTablaSalto(listaNombres);
	}

}
