import java.io.*;
public class main {
	
	public static void LeerFichero(String nombre, String[] tNombres, int[] tdni)
	{
		int resultado; //para leer
		int i; //indice tablas
		int NEmpleados; //N�Empleados del fichero
		String valorCadena; //Convetir datos leidos
		boolean numero,		//Indica si lee n�mero del fichero
				primero; 	//Indica si lee el primero numero del fichero
		
		try {
			
			File fichero = new File(nombre);
			FileReader fr = new FileReader(fichero);
			
			numero  = true;
			primero = true;
			i=0;
			valorCadena="";
			resultado = fr.read();
			while (resultado != -1)
			{
				if(resultado != '-') //Cuando es un - se va al else
					valorCadena += (char)resultado;
				else
				{
					if (numero == true)
					{
						if(primero==true)
						{
							NEmpleados = Integer.parseInt(valorCadena);
							primero = false; // para que no vuelva a leer el mismo numero

						}
						else
						{
							tdni[i] = Integer.parseInt(valorCadena);
							i++;
						}
						numero = false;
					}
					else
					{//numero = false --> Leemos un nombre
						tNombres[i] = valorCadena;
						valorCadena = "";
						numero= true;
					}
					valorCadena="";
				}
				resultado = fr.read();
			}
			fr.close();
			
		}catch(FileNotFoundException e) { System.out.println("No lee bien el fichero"); //Poniendo Exception en el catch captura todos los errores.
		}catch(IOException e) {e.printStackTrace();}
	}
	
	public static void EscribirMayorDNI(String [] tNombres, int [] DNIs)
	{
		int DNIMaximo, posMax;
		
		DNIMaximo = -99999;
		posMax=0;
		for (int i=0; i<DNIs.length;i++) {
			if (DNIs[i] > DNIMaximo)
			{
				DNIMaximo = DNIs[i];
				posMax = i;
			}
		}
		System.out.println("El nombre del DNI mayor " + tNombres[posMax]);
	}
	
	public static void EscribirPantalla(String[] tNombres, int[] tDNIs)
	{
		System.out.println("El contenido es: ");
		for (int i=0; i<tNombres.length; i++) {
			System.out.println(tNombres[i]+ " - " + tDNIs[i]);
		}
	}
	
	public static void EscribirMenorDNI(String [] tNombres, int [] DNIs)
	{
		int DNIMinimo, posMin;
		
		DNIMinimo = 1000000000;
		posMin=0;
		for (int i=0; i<DNIs.length;i++) {
			if ((DNIs[i] != 0) && (DNIs[i] < DNIMinimo))
			{
				DNIMinimo = DNIs[i];
				posMin = i;
			}
		}
		System.out.println("El nombre del DNI mayor " + tNombres[posMin]);
	}

	public static void main(String[] args) {
		
		File fichero = new File("examen.txt");
		
		String [] tablaNombres = new String[10];
		int    [] tablaDNIs    = new int [10];
		
		LeerFichero("examen.txt",tablaNombres,tablaDNIs);
		System.out.println("====");
		EscribirPantalla(tablaNombres,tablaDNIs);
		System.out.println("====");
		EscribirMayorDNI(tablaNombres,tablaDNIs);
		System.out.println("====");
		EscribirMenorDNI(tablaNombres,tablaDNIs);
	}

}
