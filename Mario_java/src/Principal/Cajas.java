package Principal;

import java.awt.Image;
import javax.swing.ImageIcon;

public class Cajas {

	protected String nombreImagen;
	protected Image imagen;
	protected int PosX;
	protected int PosY;
	// 0 = obstaculo - 1 = cajaSorpresa - 2 = cajaUsada
	protected int tipoCaja;
	protected int objeto;
	
	// Contructor
	public Cajas(String nombreImagen, int posX, int posY) {
		this.nombreImagen = nombreImagen;
		this.PosX = posX * Main.ANCHO_bloques;
		this.PosY = Main.suelo - posY * Main.ALTO_bloques;
		
		if (nombreImagen == "obstaculo")
			tipoCaja = 0;
		else if (nombreImagen == "cajaSorpresa") {
			tipoCaja = 1;
			meteMoneda();
		}
		else
			tipoCaja = 2;
		
		cambiaImagen();
	}
	
	// Getters and Setters
	public Image getImagen () {
		return imagen;
	}
	
	public String getNombreImagen() {
		return nombreImagen;
	}

	public void setNombreImagen(String nombreImagen) {
		this.nombreImagen = nombreImagen;
		this.imagen = cambiaImagen();
	}
	
	public int getPosX() {
		return PosX;
	}

	public int getPosY() {
		return PosY;
	}

	public int getTipoCaja() {
		return tipoCaja;
	}

	public void setTipoCaja(int tipoCaja) {
		this.tipoCaja = tipoCaja;
	}

	public int getObjeto() {
		return objeto;
	}

	public void setObjeto(int objeto) {
		this.objeto = objeto;
	}

	// Funciones
	public Image cambiaImagen() {
		imagen = new ImageIcon("src/Media/"+ nombreImagen +".png").getImage();
		return imagen;
	}
	
	public void meteMoneda() {
		objeto = 1;
	}	
}
