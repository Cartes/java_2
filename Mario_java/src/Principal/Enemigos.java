package Principal;

import java.awt.Image;
import javax.swing.ImageIcon;

public class Enemigos {

	String nombreImagen;
	Image imagen;
	int num_imagen;
	double PosX;
	double PosY;
	int PosParadaIz;
	int PosParadaDer;
	int dir;
	
	
	public Enemigos(String nombreImagen, double posX, double posY, int posParadaIz, int posParadaDer) {
		this.nombreImagen = nombreImagen;
		this.PosX = posX * Main.ANCHO_bloques;
		this.PosY = Main.suelo - posY * Main.ALTO_bloques;
		this.PosParadaIz = posParadaIz * Main.ANCHO_bloques;
		this.PosParadaDer = posParadaDer * Main.ANCHO_bloques;
		this.dir = 1;
		this.num_imagen = 0;
		cambiaImagen();
	}
	
	public Image getImagen() {
		return imagen;
	}

	public double getPosX() {
		return PosX;
	}

	public double getPosY() {
		return PosY;
	}

	public int getPosParadaIz() {
		return PosParadaIz;
	}

	public int getPosParadaDer() {
		return PosParadaDer;
	}

	public int getDir() {
		return dir;
	}
	
	public void setPosX(double posX) {
		PosX = posX;
		num_imagen++;
		if(num_imagen % 301 == 0)
			cambiaImagen();
	}

	public void setDir(int dir) {
		this.dir = dir;
	}

	public void cambiaImagen () {
		this.imagen = new ImageIcon("src/Media/enemigos/"+ nombreImagen + "" + (1 + num_imagen % 2)+ ".png").getImage();
	}
}