package Principal;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

enum formas {
	Quieto,
	Salto_Iz,
	Salto_Der,
	Izquierda1,
	Izquierda2,
	Izquierda3,
	Derecha1,
	Derecha2,
	Derecha3
}

public class Main extends JPanel {

	// JFrame
	private static JFrame marco;
	
	// Dimensiones de la ventana (JFrame)
    public final static int ANCHO = 1200;
    public final static int ALTO = 600;
    
    // Bloques
    public static int ANCHO_bloques = 48;
    public static int ALTO_bloques = 50;
    
    //Enemigos
    public static int ANCHO_Enemigo = ANCHO_bloques;
    public static int ALTO_Enemigo = ALTO_bloques;
    
    // Fondo
    public float ANCHO_fondo = ANCHO;
    public int ALTO_fondo = ALTO + 120;
	
		// Posicion de la foto del fondo
    public float x_fondo = 0;
	
		// Final del fondo
    public int x_final = 9100;
    
	// Posicion del juego en todo momento
    public float xGame = 0;
    public float yGame = 0;
	
	// Suelo donde se situara Mario
    public static int suelo = ALTO_bloques * 9 + 20;

	// Velocidad de Mario
    public double velocidad = 0.15;
    
	// Mario
    Mario mario = new Mario(0, 0, suelo, 0);
    
		// Posicion de Mario
    public float x = 0, y = suelo;
    public static int suelo_Mario = ALTO_bloques * 9 + 20; 
    
    	// Personaje (Mario)
    public static int ANCHO_Mario = ANCHO_bloques;
    public int ALTO_Mario = ALTO_bloques;
        
    	// Longitud del salto de Mario
    public double longitudSalto = ALTO_bloques * 3.5;
    	// Forma del sprite de Mario
    public int forma;
	// Para la animacion de moverse
	private int animacion = 0;
	private int animacion1 = 0;
    
    // Botones (booleand para saber si se presionan o no)
    public boolean arriba, salto, abajo, caida, izquierda, derecha, musicaOn, mover_arriba_menu, mover_abajo_menu;
	
	// Menu
    public boolean entrar, salir;
    public int menu;
    
    // Fin de la partida
    public boolean end = false;
    public boolean empieza = false;
    
	// Imagenes
    public Image fondo;
	public Image caja;
	public Image obstaculo;
	public Image seta;
	public Image flor;
	public Image imagen_menu;
	public Image mario_sprite;
    public Image enemigo;
    public Image flecha;
    public Image logo;
    public Image game_over;
    
    Music musicafondo = new Music("src/Music/mariotrvp.wav");
    
    Music musicaSalto = new Music("src/Music/salto.wav");
	// Posiciones de las cajas
	public int[][] posCajasSorpresa = new int[][] {
		// Nada es 0
		// Moneda es 1
		{9, 3, 0},
		{28, 3, 1}
	};
	
	// Posiciones de las setas
	public int[][] posSeta = new int[][] {
		{38, 0},
	};

	// Posiciones de las flores
	public int[][] posFlor = new int[][] {
		{42, 0},
	};
	
	// Posiciones de los obstaculos
	public int[][] posObstaculos = new int[][] {
		{20, 0},
		{21, 0},
		{21, 1},
		{22, 0},
		{31, 0}
	};
	
	// Clase de los enemigos
	Enemigos[] enemigos = {
			new Enemigos("goomba", 15, 0,  0, 19),
			new Enemigos("goomba", 11, 0,  0, 19),
			new Enemigos("goomba", 25, 0, 23, 30)
	};

	// Flecha del menu
	private int[][] posFlecha = new int[][] {
		{400, ALTO/2 - 35 + 50},
		{450, ALTO/2 - 35 + 100},
		{450, ALTO/2 - 35 + 150}
	};
	private double flecha_num = 999;
	
	public Main() {
    	menu = 1;
        x = 0;
        y = suelo_Mario;
        musicaOn = true;
        
        if (mario.tamanio == 1)
        	cambioTamanio("Grande");
        if (mario.tamanio == 2)
        	cambioTamanio("Flor");
        
    	fondo = new ImageIcon("src/Media/fondo-Mario.png").getImage();
    	caja = new ImageIcon("src/Media/cajaSorpresa.png").getImage();
    	obstaculo = new ImageIcon("src/Media/obstaculo.png").getImage();
    	seta = new ImageIcon("src/Media/seta.png").getImage();
    	flor = new ImageIcon("src/Media/flor_fuego.png").getImage();
    	mario_sprite = new ImageIcon("src/Media/sprite_Mario.png").getImage();
    	enemigo = new ImageIcon("src/Media/goomba.png").getImage();
    	imagen_menu = new ImageIcon("src/Media/imagen-menu.png").getImage();
    	logo = new ImageIcon("src/Media/mario_logo.png").getImage();
    	game_over = new ImageIcon("src/Media/game_over.png").getImage();
    	flecha = new ImageIcon("src/Media/flecha.png").getImage();
    	
    	
    	// Crea unas dimensiones de donde se va a actuar
    	// El JFrame es todo pero se pone todo en el JPanel
        setPreferredSize(new Dimension(ANCHO, ALTO));
        setFocusable(true);

    	addKeyListener(new KeyAdapter() {
        		// Hay que poner como minimo estos dos eventos
        		// Pressed actua cuando se presiona 
        		// Released actua cuando se deja de presionar
                public void keyPressed(KeyEvent e) {
                    actualiza(e.getKeyCode(), true);
                    actualiza_musica(e.getKeyCode());
                }
                
                public void keyReleased(KeyEvent e) {
                    actualiza(e.getKeyCode(), false);
                }

                private void actualiza(int keyCode, boolean pressed) {
                	// Botones menu
                	if(!empieza) {
                		switch (keyCode) {
                        	case KeyEvent.VK_UP:
                        	case KeyEvent.VK_W:
                        		flecha_num -= 0.5;
                        		break;
                        		
                        	case KeyEvent.VK_DOWN:
                        	case KeyEvent.VK_S:
                        		flecha_num += 0.5;
                            	break;
                            	
                        	case KeyEvent.VK_ENTER:
                        		entrar = true;
                        		break;
                        		
                        	case KeyEvent.VK_ESCAPE:
                        		salir = true;
                        		break;
                    	}
                	}
                	// Botones juego
                	else {
                		switch (keyCode) {
                		
                    		case KeyEvent.VK_UP:
                    		case KeyEvent.VK_W:
                    		case KeyEvent.VK_SPACE:
                    			if (!caida)
                    				salto = true;
                    			break;
                    			
                    		case KeyEvent.VK_LEFT:
                    		case KeyEvent.VK_A:
                    			izquierda = pressed;
                    			break;
                    			
                    		case KeyEvent.VK_RIGHT:
                    		case KeyEvent.VK_D:
                    			derecha = pressed;
                    			break;
                		}
                	}
                }
                
                private void actualiza_musica(int keyCode) {
                    switch (keyCode) {
                    case KeyEvent.VK_O:
                    	if (!musicaOn) {
                           	musicafondo.play();
                           	musicaOn = true;
                        }
                    	else {
                    		musicafondo.stop();
                    		musicaOn = false;
                       	}
                    break;

                    case KeyEvent.VK_UP:
                    case KeyEvent.VK_W:
                    case KeyEvent.VK_SPACE:
                    	if (salto && musicaOn && !caida) {
                           	musicaSalto.play();
                        }
                    break;
                    }
                }
            });
    }

	public static void main (String[] args) throws Exception {
    	
    	marco = new JFrame("Mario Bros");
        // NO hace falta pero esta bien ponerlo
    	marco.addWindowListener(new WindowAdapter() {
    		public void windowClosing(WindowEvent e) {
    			System.exit(0);
    		}
    	});
    	marco.setResizable(false);
    	marco.setEnabled(true);
    	marco.setSize(new Dimension(ANCHO, ALTO));
    	marco.setLocationRelativeTo(null);
    	Main comienzo = new Main();
        marco.getContentPane().add(comienzo);
        marco.pack();
        marco.setVisible(true);
        comienzo.cicloJuego();        
    }
    
	public void paint(Graphics g) {
    	super.paint(g);
    	
    	if (!empieza) {
    		if (menu == 1)
    			menuP (g);
    		else if (menu == 2)
    			menuControles (g);
    		else if (menu == 3)
    			menuCreditos (g);
    		
    		// Musica
    		g.setFont(new Font("TimesRoman", Font.BOLD, 25));
    		g.setColor(Color.RED);
    		g.drawString("Pulse O para habilitar musica", 0, 25);
    	}
    	else if (!end)
    		juego (g);
    	else
    		fin (g);
    }

    public void menuP (Graphics g) {
    	
		g.setColor(Color.black);
		g.fillRect(0, 0, ANCHO, ALTO);
		// g.drawImage(imagen_menu, 0, 0, ANCHO, ALTO, null);
		
		// Titulo
		g.setFont(new Font("TimesRoman", Font.BOLD, 40));
		g.setColor(Color.RED);
		// g.drawString("SUPER MARIO BROS", 400, 130);
		g.drawImage(logo, 300, 50, 600, 200, null);
		
		// Flecha
		g.drawImage(flecha, posFlecha[(int) flecha_num % posFlecha.length][0], posFlecha[(int) flecha_num % posFlecha.length][1], 70, 50, null);
		
		// Menus
		g.setColor(Color.RED);
		// Empezaro
		g.setFont(new Font("TimesRoman", Font.BOLD, 27));
		g.drawString("Empezar a JUGAR", 495, ALTO/2 + 50);
		// CreditosO
		g.setColor(Color.CYAN);
		g.drawString("Controles", 550, ALTO/2 + 100);
		// Creditos
		g.setColor(Color.GREEN);
		g.drawString("Creditos", 560, ALTO/2 + 150);
	}
    
    public void menuCreditos (Graphics g) {
    	
		g.setColor(Color.black);
		g.fillRect(0, 0, ANCHO, ALTO);
		
		// Titulo
		g.setFont(new Font("TimesRoman", Font.BOLD, 40));
		g.setColor(Color.RED);
		// g.drawString("SUPER MARIO BROS", 400, 130);
		g.drawImage(logo, 300, 50, 600, 200, null);
		
		g.drawString("Jorge Loarte", 400, 300);
		g.drawString("Alejandro Gutierrez", 400, 350);
		g.drawString("Marta Gomez", 400, 400);
		g.drawString("Alejandro Cartes", 400, 450);
		g.drawString("Especial agradecimientos a Kacper Duda recimientemente despedido", 0, 550);
	}
    
    public void menuControles (Graphics g) {
    	
		g.setColor(Color.black);
		g.fillRect(0, 0, ANCHO, ALTO);
		
		// Titulo
		g.setFont(new Font("TimesRoman", Font.BOLD, 40));
		g.setColor(Color.RED);	
		// g.drawString("SUPER MARIO BROS", 400, 130);
		g.drawImage(logo, 300, 50, 600, 200, null);
		
		g.drawString("Salto --> UP, W, SPACE", 300, 350);
		g.drawString("Movimiento derecha --> RIGHT, D", 300, 400);
		g.drawString("Movimiento izquierda --> LEFT, A", 300, 450);
	}
    
    public void fin (Graphics g) {

		g.drawImage(game_over, 0, 0, ANCHO, ALTO, null);
	}

    public void juego (Graphics g) {
    	
		// Fondo
		g.drawImage(fondo, 0, 0, ANCHO, ALTO, Math.round(x_fondo), 0, Math.round(ANCHO_fondo), Math.round(ALTO_fondo), null);
		
		// Mario
		//g.setColor(Color.BLACK);
		//g.fillRect(Math.round(x), Math.round(y), ANCHO_Mario, ALTO_Mario);
		g.drawImage(mario_sprite, (int) x, (int) y, (int) x + ANCHO_Mario, (int) y + ALTO_Mario,
				mario.forma_Mario[mario.getTamanio()][forma][0],
				mario.forma_Mario[mario.getTamanio()][forma][1],
				mario.forma_Mario[mario.getTamanio()][forma][2],
				mario.forma_Mario[mario.getTamanio()][forma][3],
				null);
		
		// Dibujando cajas sorpresa
		for (int i = 0; i < posCajasSorpresa.length; i++)
			dibujaBloques(g, caja, ANCHO_bloques * posCajasSorpresa[i][0], suelo - ALTO_bloques * posCajasSorpresa[i][1], ANCHO_bloques,ALTO_bloques);

		// Dibujando obstaculos
		for (int i = 0; i < posObstaculos.length; i++)
			dibujaBloques(g, obstaculo, ANCHO_bloques * posObstaculos[i][0], suelo - ALTO_bloques * posObstaculos[i][1], ANCHO_bloques,ALTO_bloques);
		
		// Dibuja Seta
		dibujaBloques(g, seta, ANCHO_bloques * posSeta[0][0], suelo + 4, ANCHO_bloques, ALTO_bloques);

		// Dibuja Flor
		dibujaBloques(g, flor, ANCHO_bloques * posFlor[0][0], suelo + 4, ANCHO_bloques, ALTO_bloques);
		
		for (int i = 0; i < enemigos.length; i++)
			dibujaBloques(g, enemigos[i].getImagen(),
					(int) enemigos[i].getPosX(), (int) enemigos[i].getPosY(),
					ANCHO_bloques, ALTO_bloques);
	}
    
    public void dibujaBloques(Graphics g, Image img, int xObject, int yObject, int width, int height) {
    	float xMin = (xGame - x);
    	float xMax = xMin + ANCHO;
    	if ( xObject + width >= xMin && xObject <= xMax )
    		g.drawImage(img, (int) (xObject - xGame + x), (int) (yObject - yGame), width, height, null);
    }

    // Funcion para repintar lo que hay en el paint
    private void dibuja() throws Exception {
		SwingUtilities.invokeAndWait(new Runnable() {
			public void run() {
				repaint();
			}

		});
    } 
    
    // Funcion del ciclo    
    public void cicloJuego() throws Exception {
      //  musicafondo.play();
    	while (!empieza) {
            dibuja();
            if (entrar) {
            	if (flecha_num % posFlecha.length == 0)
            		// Empieza el juego
            		empieza = true;
            	else if (flecha_num % posFlecha.length == 1) {
            		// Controles
            		menu = 2;
            		entrar = false;
            	}
            	else {
            		// Creditos
            		menu = 3;
        			entrar = false;
        		}
            }
            if (salir && menu != 1) {
            	// Menu Principal
            	menu = 1;
            	salir = false;
            }
    	}
    	
    	while (!end) {
        	movimientos();
        	movimientos_Enemigos();
        	if (choque_enemigo())
        		end = true;
        	formas();
        	cojeSeta();
        	cojeFlor();
           	dibuja();
           	if (x_fondo >= x_final)
           		end = true;
       	}
        musicafondo.stop();
    }
    
    // Funcion con los movimientos
    private void movimientos() {
    	// Mov. ir a la Izquierda
        if (izquierda && !choque()) {
        	if (xGame > 0)
        		xGame -= velocidad;
        	if (xGame > 400) {
        		x_fondo -= velocidad;
        		ANCHO_fondo -= velocidad;
        	}
        	else if (x > 0)
        		x -= velocidad;
        }
        else if (izquierda)
        	salto = false;
        
        // Mov. ir a la derecha
        if (derecha && !choque()) {
        	xGame += velocidad;
        	if (xGame > 400) {
        		x_fondo += velocidad;
        		ANCHO_fondo += velocidad;
        	}
        	else
        		x += velocidad;
        }
        else if (derecha)
        	salto = false;
        
        // Mov. salto
        if (salto && !choque()) {
        	y -= velocidad;
        	caida = false;
        	if (y < suelo_Mario - longitudSalto) {
        		salto = false;
        		caida = true;
        		longitudSalto = ALTO_Mario * 3;
        	}
        }
        
        // Mov. caida
        if (caida)
        	caidaFun();
        else if (!salto) {
        	musicaSalto.stop();
        	verSuelo();
        }
    }

    // Movimiento de los enemigos
    public void movimientos_Enemigos () {
    	for (int i = 0; i < enemigos.length; i++) {
    		if (enemigos[i].getPosX() <= enemigos[i].getPosParadaIz())
    			enemigos[i].setDir(1);
    		else if(enemigos[i].getPosX() >= enemigos[i].getPosParadaDer())
    			enemigos[i].setDir(-1);
    	
    		enemigos[i].setPosX(enemigos[i].getPosX() + enemigos[i].getDir() * velocidad/2);
    	}
    }
    
    // Funcion de choque de mario
    public boolean choque() {
    	boolean hasChocado = false;
    	
    	// Obstaculos
    	for (int i = 0; i < posObstaculos.length; i++) {
    		if ( derecha &&
    				Math.floor(xGame / ANCHO_bloques) + 1 == posObstaculos[i][0] &&
    				suelo / ALTO_bloques - Math.floor(y / ALTO_bloques) == posObstaculos[i][1] )
    		{
    			hasChocado = true;
    		}
    		if ( izquierda &&
    				Math.ceil(xGame / ANCHO_bloques) - 1 == posObstaculos[i][0] &&
    				suelo / ALTO_bloques - Math.floor(y / ALTO_bloques) == posObstaculos[i][1] )
    		{
    			hasChocado = true;
    		}
    		if ( salto &&
    				Math.round(xGame / ANCHO_bloques) == posObstaculos[i][0] &&
    				suelo / ALTO_bloques - suelo_Mario/ALTO_Mario - longitudSalto/ALTO_Mario >= posObstaculos[i][1] )
    		{
    			longitudSalto -= ALTO_Mario;
    			hasChocado = true;    		
    		}
    	}
    	
    	// Cajas Sorpresa
    	for (int i = 0; i < posCajasSorpresa.length; i++) {
    		if ( derecha &&
    				Math.floor(xGame / ANCHO_bloques) + 1 == posCajasSorpresa[i][0] &&
    				suelo / ALTO_bloques - Math.floor(y / ALTO_bloques) == posCajasSorpresa[i][1] )
    		{
    			hasChocado = true;
    		}
    		if ( izquierda &&
    				Math.ceil(xGame / ANCHO_bloques) - 1 == posCajasSorpresa[i][0] &&
    				suelo / ALTO_bloques - Math.floor(y / ALTO_bloques) == posCajasSorpresa[i][1] )
    		{
    			hasChocado = true;
    		}
    		if ( salto &&
    				Math.round(xGame / ANCHO_bloques) == posCajasSorpresa[i][0] &&
    				suelo / ALTO_bloques - suelo_Mario/ALTO_Mario + longitudSalto/ALTO_Mario >= posCajasSorpresa[i][1] )
    		{
    			longitudSalto -= ALTO_Mario;
    			hasChocado = true;    		
    		}
    		if ( caida &&
    				Math.floor(xGame / ANCHO_bloques) == posCajasSorpresa[i][0] &&
    				suelo / ALTO_bloques - suelo_Mario/ALTO_Mario == posCajasSorpresa[i][1] )
    		{
    			suelo_Mario = (9 - posCajasSorpresa[i][1] - 1) * ALTO_bloques + 20;
    		}
    	}
    	return hasChocado;
    }
    
    boolean choque_enemigo () {
    	for (int i = 0; i < enemigos.length; i++) {
    		if (Math.floor(xGame / ANCHO_bloques) == Math.floor(enemigos[i].getPosX() / ANCHO_bloques) &&
    			Math.round(y / ALTO_bloques) == Math.round(enemigos[i].getPosY() / ALTO_bloques) )
    			return true;
    	}
    	return false;
    }
    
    // Funciones de la caida
    public void caidaFun() {
    	boolean sigoCayendo = true;
    	for (int i = 0; i < posObstaculos.length && caida; i++) {
    		if (Math.round(xGame / ANCHO_bloques) == posObstaculos[i][0] &&
    				suelo / ALTO_bloques - suelo_Mario/ALTO_Mario == posObstaculos[i][1] )
    		{
    			suelo_Mario = (9 - posObstaculos[i][1] - 1) * ALTO_bloques + 20;
    		}
    	}
    	for (int i = 0; i < posCajasSorpresa.length && caida; i++) {
    		if (Math.round(xGame / ANCHO_bloques) == posCajasSorpresa[i][0] &&
    				suelo / ALTO_bloques - suelo_Mario/ALTO_Mario == posCajasSorpresa[i][1] )
    		{
    			suelo_Mario = (9 - posCajasSorpresa[i][1] - 1) * ALTO_bloques + 20;
    		}
    	}
    	if (sigoCayendo && y < suelo_Mario)
    		y += velocidad;
    	else
    		caida = false;
    }
    
    // Funcion para comprobar si estas en el suelo correcto
    public void verSuelo() {
    	boolean haySuelo = false;
    	for (int i = 0; i < posObstaculos.length; i++) {
    		if (Math.round(xGame / ANCHO_bloques) == posObstaculos[i][0] && 9 - suelo_Mario / ALTO_Mario - 1 == posObstaculos[i][1] ) {
    			haySuelo = true;
    			break;
    		}
    	}
    	if (!haySuelo && mario.getTamanio() == 0) {
    		suelo_Mario = suelo;
    		caida = true;
    		caidaFun();
    	}
    	if (!haySuelo && ( mario.getTamanio() == 1) || (mario.getTamanio() == 2) ) {
    		suelo_Mario = suelo - ALTO_Mario/2;
    		caida = true;
    		caidaFun();
    	}
    }
    
    // Funcion cuando cojes seta
    public void cojeSeta() {
    	if (Math.round(xGame / ANCHO_bloques) == posSeta[0][0] &&
    			suelo_Mario / ALTO_bloques - Math.round(y / ALTO_bloques) == posSeta[0][1]) {
    		if (mario.getTamanio() == 0) {
    			posSeta[0][0] = -10;
    			cambioTamanio("Grande");
    		}
    	}
    }
    
    // Funcion cuando cojes seta
    public void cojeFlor() {
    	if (Math.round(xGame / ANCHO_bloques) == posFlor[0][0] &&
    			suelo_Mario / ALTO_bloques - Math.round(y / ALTO_bloques) == posFlor[0][1]) {
    		if (mario.getTamanio() == 0 || mario.getTamanio() == 1) {
    			posFlor[0][0] = -10;
    			cambioTamanio("Fuego");
    		}
    	}
    }
    
    // Formas de Mario
    public void formas() {
    	
    	if (derecha && !salto) {
    		if (animacion % 3 == 0)
    			forma = formas.Derecha1.ordinal();
    		else if (animacion % 3 == 1)
    			forma = formas.Derecha2.ordinal();
    		else
    			forma = formas.Derecha3.ordinal();
    	}
    	
    	if (izquierda && !salto) {
    		if (animacion % 3 == 0)
    			forma = formas.Izquierda1.ordinal();
    		else if (animacion % 3 == 1)
    			forma = formas.Izquierda2.ordinal();
    		else
    			forma = formas.Izquierda3.ordinal();
    	}

		animacion1++;
		if(animacion1 % 300 == 0)
			animacion++;
		
    	if ( (!derecha && !izquierda) || (derecha && izquierda) )
    		forma = formas.Quieto.ordinal();
    	
    	if (salto || caida)
    		forma = formas.Salto_Der.ordinal();
    }
    
    // Funcion para cambiar tamanio
    public void cambioTamanio(String size) {
    	
    	if (size == "Peque") {
			mario.setTamanio(0);
    		ALTO_Mario = ALTO_bloques;
    		suelo_Mario = suelo;
			y = suelo_Mario;
    	}
    	
    	if (size == "Grande") {
			mario.setTamanio(1);
	        ALTO_Mario = ALTO_bloques * 2;
	        suelo_Mario = suelo - ALTO_Mario/2;
			y = suelo_Mario;
    	}
    	
    	if (size == "Fuego") {
			mario.setTamanio(2);
	        ALTO_Mario = ALTO_bloques * 2;
	        suelo_Mario = suelo - ALTO_Mario/2;
			y = suelo_Mario;
    	}  
    }
}