package Principal;

public class Mario {

	// Ancho y Alto de Mario
	protected int anchoMario = Main.ANCHO_bloques;
	protected int altoMario = Main.ALTO_bloques;
	
	// Posicion Mario
	protected double x;
	protected double y;
	protected double suelo_Mario;
    	
	// Longitud de Salto hacia arriba
	protected double longitudSalto;
	
	// Tamanio
		// 0 = Peque�o
		// 1 = Grande
		// 2 = Con bonificaci�n (Fire)
	protected int tamanio = 0;


	// Formas de Mario
    public int[][][] forma_Mario = new int[][][] {
    	{ 	// Peque�o
    		{50*1, 48*1, 50*2, 48*2}, // Quieto
			{50*0, 48*0, 50*1, 48*1}, // Salto Izquierda
			{50*1, 48*0, 50*2, 48*1}, // Salto Derecha
			{50*0, 48*2, 50*1, 48*3}, // Izquierda1
			{50*0, 48*3, 50*1, 48*4}, // Izquierda2
			{50*0, 48*4, 50*1, 48*5}, // Izquierda3
			{50*1, 48*2, 50*2, 48*3}, // Derecha1
			{50*1, 48*3, 50*2, 48*4}, // Derecha2
			{50*1, 48*4, 50*2, 48*5}  // Derecha3
    	},
    	{	// Grande
    		{48*4, 48*2, 48*5, 48*4},  // Quieto
    		{48*3, 48*0, 48*4, 48*2},  // Salto Izquierda
    		{48*4, 48*0, 48*5, 48*2},  // Salto Derecha
    		{48*3, 48*4, 48*4, 48*6},  // Izquierda1
    		{48*3, 48*6, 48*4, 48*8},  // Izquierda2
    		{48*3, 48*8, 48*4, 48*10}, // Izquierda3
    		{48*4, 48*4, 48*5, 48*6},  // Derecha1
    		{48*4, 48*6, 48*5, 48*8},  // Derecha2
    		{48*4, 48*8, 48*5, 48*10}  // Derecha3
        },
    	{	// Fire
    		{48*7, 48*2, 48*8, 48*4},  // Quieto
    		{48*6, 48*0, 48*7, 48*2},  // Salto Izquierda
    		{48*7, 48*0, 48*8, 48*2},  // Salto Derecha
    		{48*6, 48*4, 48*7, 48*6},  // Izquierda1
    		{48*6, 48*6, 48*7, 48*8},  // Izquierda2
    		{48*6, 48*8, 48*7, 48*10}, // Izquierda3
    		{48*7, 48*4, 48*8, 48*6},  // Derecha1
    		{48*7, 48*6, 48*8, 48*8},  // Derecha2
    		{48*7, 48*8, 48*8, 48*10}  // Derecha3
        }
	};
	
	public Mario(double posX, double posY, double suelo, int tamanio) {
		this.x = posX;
		this.y = posY;
		this.suelo_Mario = suelo;
		this.tamanio = tamanio;
		this.longitudSalto = Main.ALTO_bloques * 3;
	}

	public int getAnchoMario() {
		return anchoMario;
	}

	public int getAltoMario() {
		return altoMario;
	}

	public void setAltoMario(int alto) {
		this.altoMario = alto;
	}

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}

	public double getSuelo_Mario() {
		return suelo_Mario;
	}

	public void setSuelo_Mario(double suelo_Mario) {
		this.suelo_Mario = suelo_Mario;
	}

	public double getLongitudSalto() {
		return longitudSalto;
	}

	public void setLongitudSalto(double longitudSalto) {
		this.longitudSalto = longitudSalto;
	}

	public int getTamanio() {
		return tamanio;
	}

	public void setTamanio(int tamanio) {
		this.tamanio = tamanio;
	}

	public int[][][] getForma_Mario() {
		return forma_Mario;
	}

	public void setForma_Mario(int[][][] forma_Mario) {
		this.forma_Mario = forma_Mario;
	}
	
}