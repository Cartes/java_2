import java.awt.EventQueue;



import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.util.InputMismatchException;
import java.awt.event.ActionEvent;
import javax.swing.JComboBox;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.SwingConstants;

public class Ejercicio {

	private JFrame frmAreas;
	private JTextField radio_circulo;
	private JTextField altura_rectangulo;
	private JTextField base_rectangulo;
	private JTextField altura_triangulo;
	private JTextField base_triangulo;
	private JTextField altura_trapecio;
	private JTextField Bmayor_trapecio;
	private JTextField Bmenor_trapecio;
	private int variable_1;
	private double resultado;
	String res = String.valueOf(resultado);

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ejercicio window = new Ejercicio();
					window.frmAreas.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Ejercicio() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		JTextField lado_cuadrado;

		frmAreas = new JFrame();
		frmAreas.setTitle("Calcular Areas");
		frmAreas.setBounds(100, 100, 761, 487);
		frmAreas.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmAreas.getContentPane().setLayout(null);
		
		JButton Cuadrado = new JButton("Cuadrado");

		Cuadrado.setBounds(41, 60, 89, 23);
		frmAreas.getContentPane().add(Cuadrado);
		
		JButton Circulo = new JButton("Circulo");
		
		Circulo.setBounds(41, 99, 89, 23);
		frmAreas.getContentPane().add(Circulo);
		
		JButton Rectangulo = new JButton("Rectangulo");

		Rectangulo.setBounds(41, 133, 89, 23);
		frmAreas.getContentPane().add(Rectangulo);
		
		JButton Triangulo = new JButton("Triangulo");

		Triangulo.setBounds(41, 165, 89, 23);
		frmAreas.getContentPane().add(Triangulo);
		
		JButton Trapecio = new JButton("Trapecio");

		Trapecio.setBounds(41, 199, 89, 23);
		frmAreas.getContentPane().add(Trapecio);
		
		JTextArea Resultado = new JTextArea();
		Resultado.setBounds(41, 272, 324, 94);
		frmAreas.getContentPane().add(Resultado);
		
		JButton btnSalir = new JButton("Salir");

		btnSalir.setBounds(41, 374, 89, 23);
		frmAreas.getContentPane().add(btnSalir);
		
		lado_cuadrado = new JTextField();
		lado_cuadrado.setBounds(221, 61, 86, 20);
		frmAreas.getContentPane().add(lado_cuadrado);
		lado_cuadrado.setColumns(10);
		
		JLabel lblDimeElLado = new JLabel("Dime el lado");
		lblDimeElLado.setHorizontalAlignment(SwingConstants.LEFT);
		lblDimeElLado.setBounds(140, 64, 75, 14);
		frmAreas.getContentPane().add(lblDimeElLado);
		
		JLabel lblNewLabel = new JLabel("Dime el radio");
		lblNewLabel.setBounds(140, 103, 75, 14);
		frmAreas.getContentPane().add(lblNewLabel);
		
		radio_circulo = new JTextField();
		radio_circulo.setBounds(221, 100, 86, 20);
		frmAreas.getContentPane().add(radio_circulo);
		radio_circulo.setColumns(10);
		
		altura_rectangulo = new JTextField();
		altura_rectangulo.setBounds(211, 134, 86, 20);
		frmAreas.getContentPane().add(altura_rectangulo);
		altura_rectangulo.setColumns(10);
		
		JLabel lblNewLabel_1 = new JLabel("Base");
		lblNewLabel_1.setBounds(309, 137, 56, 14);
		frmAreas.getContentPane().add(lblNewLabel_1);
		
		base_rectangulo = new JTextField();
		base_rectangulo.setBounds(377, 134, 86, 20);
		frmAreas.getContentPane().add(base_rectangulo);
		base_rectangulo.setColumns(10);
		
		JLabel lblNewLabel_2 = new JLabel("Altura");
		lblNewLabel_2.setBounds(141, 137, 46, 14);
		frmAreas.getContentPane().add(lblNewLabel_2);
		
		JLabel label = new JLabel("Altura");
		label.setBounds(140, 169, 46, 14);
		frmAreas.getContentPane().add(label);
		
		altura_triangulo = new JTextField();
		altura_triangulo.setColumns(10);
		altura_triangulo.setBounds(211, 166, 86, 20);
		frmAreas.getContentPane().add(altura_triangulo);
		
		JLabel label_1 = new JLabel("Base");
		label_1.setBounds(309, 169, 56, 14);
		frmAreas.getContentPane().add(label_1);
		
		base_triangulo = new JTextField();
		base_triangulo.setColumns(10);
		base_triangulo.setBounds(377, 166, 86, 20);
		frmAreas.getContentPane().add(base_triangulo);
		
		JLabel label_2 = new JLabel("Altura");
		label_2.setBounds(140, 203, 46, 14);
		frmAreas.getContentPane().add(label_2);
		
		altura_trapecio = new JTextField();
		altura_trapecio.setColumns(10);
		altura_trapecio.setBounds(211, 200, 86, 20);
		frmAreas.getContentPane().add(altura_trapecio);
		
		JLabel lblBaseMayor = new JLabel("Base Mayor");
		lblBaseMayor.setBounds(309, 203, 68, 14);
		frmAreas.getContentPane().add(lblBaseMayor);
		
		Bmayor_trapecio = new JTextField();
		Bmayor_trapecio.setColumns(10);
		Bmayor_trapecio.setBounds(377, 200, 86, 20);
		frmAreas.getContentPane().add(Bmayor_trapecio);
		
		Bmenor_trapecio = new JTextField();
		Bmenor_trapecio.setColumns(10);
		Bmenor_trapecio.setBounds(558, 200, 86, 20);
		frmAreas.getContentPane().add(Bmenor_trapecio);
		
		JLabel lblBaseMenor = new JLabel("Base Menor");
		lblBaseMenor.setBounds(473, 203, 75, 14);
		frmAreas.getContentPane().add(lblBaseMenor);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(459, 272, 148, 154);
		frmAreas.getContentPane().add(scrollPane);
		
		JTextArea txtrTienesContratosHasta = new JTextArea();
		txtrTienesContratosHasta.setWrapStyleWord(true);
		scrollPane.setViewportView(txtrTienesContratosHasta);
		txtrTienesContratosHasta.setLineWrap(true);
		txtrTienesContratosHasta.setText("tienes contratos hasta las partes de los cojones");
		
		
		//Eventos
		
		Cuadrado.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String lado = lado_cuadrado.getText();
				double lado2 = Double.parseDouble(lado);
				resultado = Math.pow(lado2, 2);
				Resultado.setText(Double.toString(resultado));
			
			}
		});
		
		Circulo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String radio = radio_circulo.getText();
				double Radio = Double.parseDouble(radio);
				resultado= (Math.pow(Radio, 2) * Math.PI);
				Resultado.setText(Double.toString(resultado));
			}
		});
		
		Rectangulo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String altura = altura_rectangulo.getText();
				String base = base_rectangulo.getText();
				double Altura = Double.parseDouble(altura);
				double Base = Double.parseDouble(base);
				resultado = Altura * Base;
				Resultado.setText(Double.toString(resultado));
			}
		});
		
		
		Triangulo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String altura = altura_triangulo.getText();
				String base = base_triangulo.getText();
				double Altura = Double.parseDouble(altura);
				double Base = Double.parseDouble(base);
				resultado = ((Altura * Base)/2);
				Resultado.setText(Double.toString(resultado));
				
			}
		});
		
		Trapecio.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String altura = altura_trapecio.getText();
				String Bmayor = Bmayor_trapecio.getText();
				String Bmenor = Bmenor_trapecio.getText();
				double Altura = Double.parseDouble(altura);
				double BMayor = Double.parseDouble(Bmayor);
				double BMenor = Double.parseDouble(Bmenor);
				resultado = (Altura * (BMayor+BMenor) / 2);
				Resultado.setText(Double.toString(resultado));
				
			}
		});
		
		btnSalir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		
		/*Para el funcionamiento necesitamos poner los numeros en su respectiva fila y darle al botton del cual queramos hacer la operacion*/
		
		
	}
}
