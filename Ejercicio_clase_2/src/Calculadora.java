import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.GridLayout;
import java.awt.BorderLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import java.awt.GridBagLayout;
import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Calculadora {

	private JFrame frmCalculadora;
	private JTextField Resultado;
	private char op = ' ';
	private String num1;
	private String num2;
	private int posicion;
	private double total;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Calculadora window = new Calculadora();
					window.frmCalculadora.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Calculadora() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmCalculadora = new JFrame();
		frmCalculadora.setResizable(false);
		frmCalculadora.setTitle("Calculadora");
		frmCalculadora.setBounds(100, 100, 322, 403);
		frmCalculadora.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JButton Suma = new JButton("Suma");
		
		JButton Resta = new JButton("Resta");
		
		JButton Division = new JButton("Division");
		
		JButton Multiplicacion = new JButton("Multiplicacion");
		
		JLabel lblResultado = new JLabel("Resultado");
		
		Resultado = new JTextField();
		Resultado.setEnabled(false);
		Resultado.setColumns(10);
		
		JButton uno = new JButton("1");
		
		JButton dos = new JButton("2");
		
		JButton tres = new JButton("3");
		
		JButton cuatro = new JButton("4");
		
		JButton cinco = new JButton("5");
		
		JButton seis = new JButton("6");
		
		JButton siete = new JButton("7");
		
		JButton ocho = new JButton("8");
		
		JButton nueve = new JButton("9");
		
		JButton cero = new JButton("0");
		
		JButton Igual = new JButton("=");
		
		JButton button_8 = new JButton(".");
		
		JButton borrar = new JButton("BORRAR");

		GroupLayout groupLayout = new GroupLayout(frmCalculadora.getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(uno, GroupLayout.PREFERRED_SIZE, 54, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(dos, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
						.addComponent(lblResultado, GroupLayout.DEFAULT_SIZE, 103, Short.MAX_VALUE)
						.addGroup(groupLayout.createSequentialGroup()
							.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING, false)
								.addComponent(siete, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(cuatro, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 55, Short.MAX_VALUE)
								.addComponent(button_8, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addGroup(groupLayout.createSequentialGroup()
									.addGroup(groupLayout.createParallelGroup(Alignment.LEADING, false)
										.addComponent(ocho, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
										.addComponent(cinco, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
									.addPreferredGap(ComponentPlacement.RELATED, 3, Short.MAX_VALUE))
								.addComponent(cero, GroupLayout.DEFAULT_SIZE, 42, Short.MAX_VALUE))))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addGroup(groupLayout.createSequentialGroup()
									.addGap(3)
									.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
										.addComponent(tres, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
										.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
											.addComponent(nueve, GroupLayout.DEFAULT_SIZE, 58, Short.MAX_VALUE)
											.addComponent(seis, Alignment.TRAILING, GroupLayout.PREFERRED_SIZE, 58, GroupLayout.PREFERRED_SIZE))))
								.addGroup(groupLayout.createSequentialGroup()
									.addGap(6)
									.addComponent(Igual, GroupLayout.DEFAULT_SIZE, 55, Short.MAX_VALUE)))
							.addGap(37)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING, false)
								.addComponent(Division, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(Multiplicacion, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(Resta, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(Suma, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
							.addGap(300))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(9)
							.addComponent(Resultado, GroupLayout.PREFERRED_SIZE, 139, GroupLayout.PREFERRED_SIZE)
							.addContainerGap())))
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(52)
					.addComponent(borrar)
					.addContainerGap(465, Short.MAX_VALUE))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(140)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblResultado)
						.addComponent(Resultado, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(uno, GroupLayout.PREFERRED_SIZE, 38, GroupLayout.PREFERRED_SIZE)
						.addComponent(dos, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
						.addComponent(tres, GroupLayout.PREFERRED_SIZE, 41, GroupLayout.PREFERRED_SIZE)
						.addComponent(Suma))
					.addGap(2)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(cuatro, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE)
						.addComponent(cinco, GroupLayout.PREFERRED_SIZE, 37, GroupLayout.PREFERRED_SIZE)
						.addComponent(seis, GroupLayout.PREFERRED_SIZE, 31, GroupLayout.PREFERRED_SIZE)
						.addComponent(Resta, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
					.addPreferredGap(ComponentPlacement.UNRELATED)	
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
							.addComponent(siete, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE)
							.addComponent(ocho, GroupLayout.PREFERRED_SIZE, 31, GroupLayout.PREFERRED_SIZE)
							.addComponent(nueve, GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE))
						.addComponent(Division))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(button_8)
						.addComponent(cero, GroupLayout.PREFERRED_SIZE, 33, GroupLayout.PREFERRED_SIZE)
						.addComponent(Igual, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(Multiplicacion))
					.addGap(2)
					.addComponent(borrar))
		);
		frmCalculadora.getContentPane().setLayout(groupLayout);
		
		uno.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Resultado.setText(Resultado.getText()+"1");
			}
		});
		
		dos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Resultado.setText(Resultado.getText()+"2");
			}
		});
		
		tres.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Resultado.setText(Resultado.getText()+"3");
			}
		});
		
		cuatro.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Resultado.setText(Resultado.getText()+"4");
			}
		});
		
		cinco.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Resultado.setText(Resultado.getText()+"5");
			}
		});
		
		seis.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Resultado.setText(Resultado.getText()+"6");
			}
		});
		
		siete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Resultado.setText(Resultado.getText()+"7");
			}
		});
		
		ocho.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Resultado.setText(Resultado.getText()+"8");
			}
		});
		
		nueve.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Resultado.setText(Resultado.getText()+"9");
			}
		});
		
		cero.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Resultado.setText(Resultado.getText()+"0");
			}
		});
		
		borrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Resultado.setText("");
				Suma.setEnabled(true);
				Resta.setEnabled(true);
				Multiplicacion.setEnabled(true);
				Division.setEnabled(true);
			}
		});
		
		Suma.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				op = '+';
				Resultado.setText(Resultado.getText()+"+");
				Suma.setEnabled(false);
				Resta.setEnabled(false);
				Multiplicacion.setEnabled(false);
				Division.setEnabled(false);
			}
		});
		
		Resta.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				op = '-';
				Resultado.setText(Resultado.getText()+"-");
				Suma.setEnabled(false);
				Resta.setEnabled(false);
				Multiplicacion.setEnabled(false);
				Division.setEnabled(false);
			}
		});
		
		Multiplicacion.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				op = '*';
				Resultado.setText(Resultado.getText()+"*");
				Suma.setEnabled(false);
				Resta.setEnabled(false);
				Multiplicacion.setEnabled(false);
				Division.setEnabled(false);
			}
		});
		
		Division.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				op = '/';
				Resultado.setText(Resultado.getText()+"/");
				Suma.setEnabled(false);
				Resta.setEnabled(false);
				Multiplicacion.setEnabled(false); 
				Division.setEnabled(false);
			}
		});
		
		Igual.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				switch(op) {
				case '+':
					int posicion = Resultado.getText().indexOf('+');
					num1 = Resultado.getText().substring(0, posicion);
					num2 = Resultado.getText().substring(posicion+1, Resultado.getText().length());
					double total = Double.parseDouble(num1) + Double.parseDouble(num2);
					Resultado.setText(Double.toString(total));
					break;
				case '-':
					int posicion1 = Resultado.getText().indexOf('-');
					num1 = Resultado.getText().substring(0, posicion1);
					num2 = Resultado.getText().substring(posicion1+1, Resultado.getText().length());
					double total1 = Double.parseDouble(num1) - Double.parseDouble(num2);
					Resultado.setText(Double.toString(total1));
					break;
				case '*':
					int posicion2 = Resultado.getText().indexOf('*');
					num1 = Resultado.getText().substring(0, posicion2);
					num2 = Resultado.getText().substring(posicion2+1, Resultado.getText().length());
					double total2 = Double.parseDouble(num1) * Double.parseDouble(num2);
					Resultado.setText(Double.toString(total2));
					break;
				case '/':
					int posicion3 = Resultado.getText().indexOf('/');
					num1 = Resultado.getText().substring(0, posicion3);
					num2 = Resultado.getText().substring(posicion3+1, Resultado.getText().length());
					double total3 = Double.parseDouble(num1) / Double.parseDouble(num2);
					Resultado.setText(Double.toString(total3));
					break;
					
				}
				
			}
		});
	}
}
