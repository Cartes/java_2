package main;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTextPane;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultHighlighter;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.JEditorPane;
import java.awt.SystemColor;
import java.awt.Color;
import java.awt.Component;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;
import java.awt.Font;
import java.awt.Toolkit;
import java.util.Vector;

public class partefinal {
	
	private JFrame frame;
	public JFrame getFrame() {
		return frame;
	}

	public void setFrame(JFrame frame) {
		this.frame = frame;
	}

	
	//Variables
	char car;
	static int pulsaciones;
	
	static tiempo tiempo1 = new tiempo();

	//Variables de componentes
	JButton btnNewButton_1 = new JButton("W");
	JButton btnNewButton_2 = new JButton("E");
	JButton btnNewButton_3 = new JButton("R");
	JButton btnNewButton_4 = new JButton("T");
	JButton btnNewButton_5 = new JButton("Y");
	JButton btnNewButton_6 = new JButton("U");
	JButton btnNewButton_7 = new JButton("I");
	JButton btnNewButton_8 = new JButton("O");
	JButton btnNewButton_9 = new JButton("P");
	JButton btnNewButton_10 = new JButton("A");
	JButton btnNewButton_11 = new JButton("S");
	JButton btnNewButton_12 = new JButton("D");
	JButton btnNewButton_13 = new JButton("F");
	JButton btnNewButton_14 = new JButton("G");
	JButton btnNewButton_15 = new JButton("H");
	JButton btnNewButton_16 = new JButton("J");
	JButton btnNewButton_17 = new JButton("K");
	JButton btnNewButton_18 = new JButton("L");
	JButton btnNewButton_19 = new JButton("Q");
	JButton btnNewButton_20 = new JButton("Z");
	JButton btnNewButton_21 = new JButton("X");
	JButton btnNewButton_22 = new JButton("C");
	JButton btnNewButton_23 = new JButton("V");
	JButton btnNewButton_24 = new JButton("B");
	JButton btnNewButton_25 = new JButton("N");
	JButton btnNewButton_26 = new JButton("M");
	JButton btnNewButton_28 = new JButton(".");
	JButton btnNewButton_30 = new JButton("Espacio");
	static JTextPane txtpnNoTodosMis = new JTextPane();
    static JTextPane panel = new JTextPane();
    static JEditorPane editorPane3 = new JEditorPane();
    static JEditorPane editorPane2 = new JEditorPane(); 
    static JTextPane textPane = new JTextPane();
	static JLabel tiempo = new JLabel();
	static DefaultHighlighter.DefaultHighlightPainter highlightPainterR = new DefaultHighlighter.DefaultHighlightPainter(Color.RED);
	static DefaultHighlighter.DefaultHighlightPainter highlightPainterG = new DefaultHighlighter.DefaultHighlightPainter(Color.GREEN);

	static String texto="";	
    static File archivo = null;
    static FileReader fr = null;
    static BufferedReader br = null;
    
    static int fallos = 0;
    static Scanner fichero = null;
    static int nivel1 = 0, nivel2 = 1;
    private static int contador;
    private static int suma;
    private static int i=0;
    static char [] arr= new char[500];
    

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					partefinal window = new partefinal();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	public static void leerFichero (String direccion) {
			try {
				BufferedReader bf = new BufferedReader(new FileReader(direccion));
				String tmp="";
				String bfRead = null;
				while ((bfRead = bf.readLine()) != null) {
					tmp += bfRead;
				}
				
				texto = tmp;
			}catch(Exception e) {
				System.err.println("No he encontrado el archivo");
			}
	}
	
	public static void nivel (int nivel) {
	 if (nivel == 0) {
		 leerFichero("users/archivoCorto.txt");
		 textPane.setText(texto);
	 }
	 if (nivel == 1)
	 {
		leerFichero("users/archivo.txt");
		textPane.setText(texto);
		 
	 }
	 
	}
	
	public static void Estadisticas() {
		try {
			File fichero = new File("users\\estadisticas.txt");
			FileWriter fw = null;
			fw = new FileWriter(fichero,true);
			
			fw.write(pulsaciones+"Pulsaciones");
			fw.write(fallos+"Fallos Obtenidos");
			
		}catch(Exception er) {er.printStackTrace();}
	}
	
	public static void run() {
		String cont;
		cont = String.valueOf(tiempo1.getSegundos());	
		editorPane3.setText(cont);
		System.out.println(cont);
				if (tiempo1.getSegundos() >= 2400) {
			tiempo1.Detener();
			JOptionPane.showMessageDialog(null, "Has llegado al limite");
			Estadisticas();
			System.exit(0);
		
		}
	}
	
	public void highliter() {
		if (car == arr[contador])
		{
			try {
				textPane.getHighlighter().addHighlight(suma, suma+1, highlightPainterG);
				suma++;
				}catch(Exception e1) {e1.printStackTrace();}
		}
		
		else
		{
			try {
				textPane.getHighlighter().addHighlight(suma, suma+1, highlightPainterR);
				suma++;
				fallos++;
				}catch(Exception e1) {e1.printStackTrace();}
				}
		contador++;
	}
		
	

	public partefinal() {
		initialize();
	}

	private void initialize() {
		tiempo1.Contar();
		Estadisticas();
		frame = new JFrame();
		frame.setIconImage(Toolkit.getDefaultToolkit().getImage("images/imagen1.png"));
		frame.getContentPane().setBackground(Color.ORANGE);
		frame.getContentPane().setForeground(SystemColor.activeCaptionText);
		frame.setTitle("Mecanografia");
		frame.setBounds(100, 100, 1920, 1080);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.setUndecorated(true);
		frame.setLocationRelativeTo(null);
		
		textPane.setEditable(false);
		textPane.setFont(new Font("Tahoma", Font.PLAIN, 73));
		textPane.setText(" ");
		textPane.setBounds(16, 11, 1873, 502);
		frame.getContentPane().add(textPane);
		
		JButton btnNewButton_19 = new JButton("Q");
		btnNewButton_19.setFont(new Font("Tahoma", Font.PLAIN, 25));
		btnNewButton_19.setBackground(Color.WHITE);
		btnNewButton_19.setBounds(30, 569, 147, 81);
		frame.getContentPane().add(btnNewButton_19);
		
		btnNewButton_1.setFont(new Font("Tahoma", Font.PLAIN, 25));
		btnNewButton_1.setBackground(Color.WHITE);
		btnNewButton_1.setBounds(184, 569, 147, 81);
		frame.getContentPane().add(btnNewButton_1);
		btnNewButton_2.setFont(new Font("Tahoma", Font.PLAIN, 25));
		
		
		btnNewButton_2.setBackground(Color.WHITE);
		btnNewButton_2.setBounds(337, 569, 147, 81);
		frame.getContentPane().add(btnNewButton_2);
		btnNewButton_3.setFont(new Font("Tahoma", Font.PLAIN, 25));
		
		btnNewButton_3.setBackground(Color.WHITE);
		btnNewButton_3.setBounds(489, 569, 147, 81);
		frame.getContentPane().add(btnNewButton_3);
		btnNewButton_4.setFont(new Font("Tahoma", Font.PLAIN, 25));
		
		btnNewButton_4.setBackground(Color.WHITE);
		btnNewButton_4.setBounds(642, 569, 147, 81);
		frame.getContentPane().add(btnNewButton_4);
		btnNewButton_5.setFont(new Font("Tahoma", Font.PLAIN, 25));
		
		 
		btnNewButton_5.setBackground(Color.WHITE);
		btnNewButton_5.setBounds(795, 569, 155, 81);
		frame.getContentPane().add(btnNewButton_5);
		btnNewButton_6.setFont(new Font("Tahoma", Font.PLAIN, 25));
		
		
		btnNewButton_6.setBackground(Color.WHITE);
		btnNewButton_6.setBounds(957, 568, 147, 82);
		frame.getContentPane().add(btnNewButton_6);
		btnNewButton_7.setFont(new Font("Tahoma", Font.PLAIN, 25));
		
		
		btnNewButton_7.setBackground(Color.WHITE);
		btnNewButton_7.setBounds(1113, 569, 155, 81);
		frame.getContentPane().add(btnNewButton_7);
		btnNewButton_8.setFont(new Font("Tahoma", Font.PLAIN, 25));
	
		btnNewButton_8.setBackground(Color.WHITE);
		btnNewButton_8.setBounds(1275, 569, 155, 81);
		frame.getContentPane().add(btnNewButton_8);
		btnNewButton_9.setFont(new Font("Tahoma", Font.PLAIN, 25));

		
		
		btnNewButton_9.setBackground(Color.WHITE);
		btnNewButton_9.setBounds(1435, 568, 155, 83);
		frame.getContentPane().add(btnNewButton_9);
		btnNewButton_10.setFont(new Font("Tahoma", Font.PLAIN, 25));
		btnNewButton_10.setBackground(Color.WHITE);
		
		
		btnNewButton_10.setBounds(86, 669, 147, 81);
		frame.getContentPane().add(btnNewButton_10);
		btnNewButton_11.setFont(new Font("Tahoma", Font.PLAIN, 25));
		
		
		btnNewButton_11.setBackground(Color.WHITE);
		btnNewButton_11.setBounds(243, 669, 147, 81);
		frame.getContentPane().add(btnNewButton_11);
		btnNewButton_12.setFont(new Font("Tahoma", Font.PLAIN, 25));
		
		
		btnNewButton_12.setBackground(Color.WHITE);
		btnNewButton_12.setBounds(398, 669, 147, 81);
		frame.getContentPane().add(btnNewButton_12);
		btnNewButton_13.setFont(new Font("Tahoma", Font.PLAIN, 25));
			
		btnNewButton_13.setBackground(Color.WHITE);
		btnNewButton_13.setBounds(554, 669, 147, 81);
		frame.getContentPane().add(btnNewButton_13);
		btnNewButton_14.setFont(new Font("Tahoma", Font.PLAIN, 25));
		
		
		btnNewButton_14.setBackground(Color.WHITE);
		btnNewButton_14.setBounds(711, 669, 155, 81);
		frame.getContentPane().add(btnNewButton_14);
		btnNewButton_15.setFont(new Font("Tahoma", Font.PLAIN, 25));
		
		
		btnNewButton_15.setBackground(Color.WHITE);
		btnNewButton_15.setBounds(876, 669, 158, 81);
		frame.getContentPane().add(btnNewButton_15);
		btnNewButton_16.setFont(new Font("Tahoma", Font.PLAIN, 25));
		
		
		btnNewButton_16.setBackground(Color.WHITE);
		btnNewButton_16.setBounds(1041, 669, 155, 81);
		frame.getContentPane().add(btnNewButton_16);
		btnNewButton_17.setFont(new Font("Tahoma", Font.PLAIN, 25));
		
		
		btnNewButton_17.setBackground(Color.WHITE);
		btnNewButton_17.setBounds(1206, 669, 158, 81);
		frame.getContentPane().add(btnNewButton_17);
		btnNewButton_18.setFont(new Font("Tahoma", Font.PLAIN, 25));
		
		
		btnNewButton_18.setBackground(Color.WHITE);
		btnNewButton_18.setBounds(1374, 669, 155, 81);
		frame.getContentPane().add(btnNewButton_18);
		btnNewButton_20.setFont(new Font("Tahoma", Font.PLAIN, 25));
		
		
		btnNewButton_20.setBackground(Color.WHITE);
		btnNewButton_20.setBounds(30, 761, 147, 81);
		frame.getContentPane().add(btnNewButton_20);
		btnNewButton_21.setFont(new Font("Tahoma", Font.PLAIN, 25));
		
		
		btnNewButton_21.setBackground(Color.WHITE);
		btnNewButton_21.setBounds(186, 761, 147, 81);
		frame.getContentPane().add(btnNewButton_21);
		btnNewButton_22.setFont(new Font("Tahoma", Font.PLAIN, 25));
		
	
		btnNewButton_22.setBackground(Color.WHITE);
		btnNewButton_22.setBounds(341, 761, 147, 81);
		frame.getContentPane().add(btnNewButton_22);
		btnNewButton_23.setFont(new Font("Tahoma", Font.PLAIN, 25));
		
		
		btnNewButton_23.setBackground(Color.WHITE);
		btnNewButton_23.setBounds(498, 761, 155, 81);
		frame.getContentPane().add(btnNewButton_23);
		btnNewButton_24.setFont(new Font("Tahoma", Font.PLAIN, 25));
		
		
		btnNewButton_24.setBackground(Color.WHITE);
		btnNewButton_24.setBounds(663, 761, 147, 81);
		frame.getContentPane().add(btnNewButton_24);
		btnNewButton_25.setFont(new Font("Tahoma", Font.PLAIN, 25));
		
		
		btnNewButton_25.setBackground(Color.WHITE);
		btnNewButton_25.setBounds(820, 761, 147, 81);
		frame.getContentPane().add(btnNewButton_25);
		btnNewButton_26.setFont(new Font("Tahoma", Font.PLAIN, 25));		
		
		btnNewButton_26.setBackground(Color.WHITE);
		btnNewButton_26.setBounds(977, 761, 147, 81);
		frame.getContentPane().add(btnNewButton_26);
		
		JButton btnNewButton_27 = new JButton(",");
		btnNewButton_27.setFont(new Font("Tahoma", Font.PLAIN, 25));
		btnNewButton_27.setBackground(Color.WHITE);
		btnNewButton_27.setBounds(1134, 761, 147, 81);
		frame.getContentPane().add(btnNewButton_27);
		
		JButton btnNewButton_28 = new JButton(".");
		btnNewButton_28.setFont(new Font("Tahoma", Font.PLAIN, 25));
		btnNewButton_28.setBackground(Color.WHITE);
		btnNewButton_28.setBounds(1291, 761, 147, 81);
		frame.getContentPane().add(btnNewButton_28);
		
		JButton btnNewButton_29 = new JButton("_");
		btnNewButton_29.setFont(new Font("Tahoma", Font.PLAIN, 25));
		btnNewButton_29.setBackground(Color.WHITE);
		btnNewButton_29.setBounds(1448, 761, 147, 81);
		frame.getContentPane().add(btnNewButton_29);
		
		JLabel lblPulsacionesTotales = new JLabel("Pulsaciones totales");
		lblPulsacionesTotales.setFont(new Font("Tahoma", Font.PLAIN, 23));
		lblPulsacionesTotales.setBounds(1683, 520, 206, 41);
		frame.getContentPane().add(lblPulsacionesTotales);
		
		JEditorPane editorPane = new JEditorPane();
		editorPane.setBounds(1683, 570, 182, 41);
		frame.getContentPane().add(editorPane);
		
		JLabel lblPulsacionesminuto = new JLabel("Pulsaciones/Minuto");
		lblPulsacionesminuto.setFont(new Font("Tahoma", Font.PLAIN, 23));
		lblPulsacionesminuto.setBounds(1683, 637, 206, 24);
		frame.getContentPane().add(lblPulsacionesminuto);
		
		JEditorPane editorPane_1 = new JEditorPane();
		editorPane_1.setBounds(1683, 688, 182, 41);
		frame.getContentPane().add(editorPane_1);
		
		JLabel lblErrores = new JLabel("Errores");
		lblErrores.setFont(new Font("Tahoma", Font.PLAIN, 23));
		lblErrores.setBounds(1683, 732, 182, 39);
		frame.getContentPane().add(lblErrores);
		
		JEditorPane editorPane_2 = new JEditorPane();
		editorPane_2.setBounds(1683, 782, 182, 41);
		frame.getContentPane().add(editorPane_2);
		
		JLabel lblTiempo = new JLabel("Tiempo");
		lblTiempo.setFont(new Font("Tahoma", Font.PLAIN, 23));
		lblTiempo.setBounds(1681, 834, 184, 39);
		frame.getContentPane().add(lblTiempo);
		
		
		editorPane3.setBounds(1683, 899, 179, 41);
		frame.getContentPane().add(editorPane3);
		
		JButton btnNewButton_30 = new JButton("Espacio");
		btnNewButton_30.setFont(new Font("Tahoma", Font.PLAIN, 25));
		btnNewButton_30.setBackground(Color.WHITE);
		btnNewButton_30.setBounds(337, 851, 812, 81);
		frame.getContentPane().add(btnNewButton_30);
		
		JButton btnNewButton_34 = new JButton("Salir");
		btnNewButton_34.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				System.exit(0);
			}
		});
		btnNewButton_34.setBounds(1667, 979, 207, 70);
		frame.getContentPane().add(btnNewButton_34);
		
		char [] arr = null;
		
		
		textPane.addKeyListener(new KeyAdapter() {			@Override
			public void keyPressed(java.awt.event.KeyEvent e) {
			setArr( texto.toCharArray());
				pulsaciones++;
				String pul = String.valueOf(pulsaciones);
				editorPane.setText(pul);
				/*
				String minuto;
				minuto = String.valueOf((60 * pulsaciones)/tiempo1.getSegundos()+1);
				editorPane_1.setText(minuto);
*/
				
				
				int fallos=0;
				
				char[] resultado = getArr();
				
				
				if (contador<resultado.length) {
					
					if (e.getKeyCode() == 32) {
						i=0;
					}
					else {
						i=32;
					}
				}
				car = ((char)(e.getKeyCode()+i));
				if (car == resultado[contador])
				{
					try {
						textPane.getHighlighter().addHighlight(suma, suma+1, highlightPainterG);
						suma++;
						if (e.getKeyCode() == KeyEvent.VK_W) {
							btnNewButton_1.setBackground(Color.GREEN);
						}
						else if(e.getKeyCode() == KeyEvent.VK_E) {
							btnNewButton_2.setBackground(Color.GREEN);
						}
						else if (e.getKeyCode() == KeyEvent.VK_R) {
							btnNewButton_3.setBackground(Color.GREEN);
						}
						else if (e.getKeyCode() == KeyEvent.VK_T) {
							btnNewButton_4.setBackground(Color.GREEN);
						}
						else if (e.getKeyCode() == KeyEvent.VK_Y) {
							btnNewButton_5.setBackground(Color.GREEN);
						}
						else if (e.getKeyCode() == KeyEvent.VK_U) {
							btnNewButton_6.setBackground(Color.GREEN);
						}
						else if (e.getKeyCode() == KeyEvent.VK_I) {
							btnNewButton_7.setBackground(Color.GREEN);
						}
						else if (e.getKeyCode() == KeyEvent.VK_O) {
							btnNewButton_8.setBackground(Color.GREEN);
						}
						else if (e.getKeyCode() == KeyEvent.VK_P) {
							btnNewButton_9.setBackground(Color.GREEN);
						}
						else if (e.getKeyCode() == KeyEvent.VK_A) {
							btnNewButton_10.setBackground(Color.GREEN);
						}
						else if (e.getKeyCode() == KeyEvent.VK_S) {
							btnNewButton_11.setBackground(Color.GREEN);
						}
						else if (e.getKeyCode() == KeyEvent.VK_D) {
							btnNewButton_12.setBackground(Color.GREEN);
						}
						else if (e.getKeyCode() == KeyEvent.VK_F) {
							btnNewButton_13.setBackground(Color.GREEN);
						}
						else if (e.getKeyCode() == KeyEvent.VK_G) {
							btnNewButton_14.setBackground(Color.GREEN);
						}
						else if (e.getKeyCode() == KeyEvent.VK_H) {
							btnNewButton_15.setBackground(Color.GREEN);
						}
						else if (e.getKeyCode() == KeyEvent.VK_J) {
							btnNewButton_16.setBackground(Color.GREEN);
						}
						else if (e.getKeyCode() == KeyEvent.VK_K) {
							btnNewButton_17.setBackground(Color.GREEN);
						}
						else if (e.getKeyCode() == KeyEvent.VK_L) {
							btnNewButton_18.setBackground(Color.GREEN);
						}
						else if (e.getKeyCode() == KeyEvent.VK_Q) {
							btnNewButton_19.setBackground(Color.GREEN);
						}
						else if (e.getKeyCode() == KeyEvent.VK_Z) {
							btnNewButton_20.setBackground(Color.GREEN);
						}
						else if (e.getKeyCode() == KeyEvent.VK_X) {
							btnNewButton_21.setBackground(Color.GREEN);
						}
						else if (e.getKeyCode() == KeyEvent.VK_C) {
							btnNewButton_22.setBackground(Color.GREEN);
						}
						else if (e.getKeyCode() == KeyEvent.VK_V) {
							btnNewButton_23.setBackground(Color.GREEN);
						}
						else if (e.getKeyCode() == KeyEvent.VK_B) {
							btnNewButton_24.setBackground(Color.GREEN);
						}
						else if (e.getKeyCode() == KeyEvent.VK_N) {
							btnNewButton_25.setBackground(Color.GREEN);
						}
						else if (e.getKeyCode() == KeyEvent.VK_M) {
							btnNewButton_26.setBackground(Color.GREEN);
						}
						else if (e.getKeyCode() == KeyEvent.VK_PERIOD || e.getKeyCode() == KeyEvent.VK_DECIMAL){
							btnNewButton_28.setBackground(Color.GREEN);
						}
						else if (e.getKeyCode() == KeyEvent.VK_SPACE){
							btnNewButton_30.setBackground(Color.GREEN);
						}
						
						}catch(Exception e1) {e1.printStackTrace();}
				
				}
				
				else
				{
					try {
						textPane.getHighlighter().addHighlight(suma, suma+1, highlightPainterR);
						suma++;
						if (e.getKeyCode() == KeyEvent.VK_W) {
							btnNewButton_1.setBackground(Color.RED);
						}
						else if(e.getKeyCode() == KeyEvent.VK_E) {
							btnNewButton_2.setBackground(Color.RED);
						}
						else if (e.getKeyCode() == KeyEvent.VK_R) {
							btnNewButton_3.setBackground(Color.RED);
						}
						else if (e.getKeyCode() == KeyEvent.VK_T) {
							btnNewButton_4.setBackground(Color.RED);
						}
						else if (e.getKeyCode() == KeyEvent.VK_Y) {
							btnNewButton_5.setBackground(Color.RED);
						}
						else if (e.getKeyCode() == KeyEvent.VK_U) {
							btnNewButton_6.setBackground(Color.RED);
						}
						else if (e.getKeyCode() == KeyEvent.VK_I) {
							btnNewButton_7.setBackground(Color.RED);
						}
						else if (e.getKeyCode() == KeyEvent.VK_O) {
							btnNewButton_8.setBackground(Color.RED);
						}
						else if (e.getKeyCode() == KeyEvent.VK_P) {
							btnNewButton_9.setBackground(Color.RED);
						}
						else if (e.getKeyCode() == KeyEvent.VK_A) {
							btnNewButton_10.setBackground(Color.RED);
						}
						else if (e.getKeyCode() == KeyEvent.VK_S) {
							btnNewButton_11.setBackground(Color.RED);
						}
						else if (e.getKeyCode() == KeyEvent.VK_D) {
							btnNewButton_12.setBackground(Color.RED);
						}
						else if (e.getKeyCode() == KeyEvent.VK_F) {
							btnNewButton_13.setBackground(Color.RED);
						}
						else if (e.getKeyCode() == KeyEvent.VK_G) {
							btnNewButton_14.setBackground(Color.RED);
						}
						else if (e.getKeyCode() == KeyEvent.VK_H) {
							btnNewButton_15.setBackground(Color.RED);
						}
						else if (e.getKeyCode() == KeyEvent.VK_J) {
							btnNewButton_16.setBackground(Color.RED);
						}
						else if (e.getKeyCode() == KeyEvent.VK_K) {
							btnNewButton_17.setBackground(Color.RED);
						}
						else if (e.getKeyCode() == KeyEvent.VK_L) {
							btnNewButton_18.setBackground(Color.RED);
						}
						else if (e.getKeyCode() == KeyEvent.VK_Q) {
							btnNewButton_19.setBackground(Color.RED);
						}
						else if (e.getKeyCode() == KeyEvent.VK_Z) {
							btnNewButton_20.setBackground(Color.RED);
						}
						else if (e.getKeyCode() == KeyEvent.VK_X) {
							btnNewButton_21.setBackground(Color.RED);
						}
						else if (e.getKeyCode() == KeyEvent.VK_C) {
							btnNewButton_22.setBackground(Color.RED);
						}
						else if (e.getKeyCode() == KeyEvent.VK_V) {
							btnNewButton_23.setBackground(Color.RED);
						}
						else if (e.getKeyCode() == KeyEvent.VK_B) {
							btnNewButton_24.setBackground(Color.RED);
						}
						else if (e.getKeyCode() == KeyEvent.VK_N) {
							btnNewButton_25.setBackground(Color.RED);
						}
						else if (e.getKeyCode() == KeyEvent.VK_M) {
							btnNewButton_26.setBackground(Color.RED);
						}
						else if (e.getKeyCode() == KeyEvent.VK_PERIOD || car == KeyEvent.VK_DECIMAL){
							btnNewButton_28.setBackground(Color.RED);
						}
						else if (e.getKeyCode() == KeyEvent.VK_SPACE){
							btnNewButton_30.setBackground(Color.RED);
						}	
						fallos++;
						}catch(Exception e1) {e1.printStackTrace();}
				}
				contador++;
				editorPane_2.setText(String.valueOf(fallos));
		}
	
	public void keyReleased(java.awt.event.KeyEvent e) {
			btnNewButton_1.setBackground(Color.WHITE);
			btnNewButton_2.setBackground(Color.WHITE);
			btnNewButton_3.setBackground(Color.WHITE);
			btnNewButton_4.setBackground(Color.WHITE);
			btnNewButton_5.setBackground(Color.WHITE);
			btnNewButton_6.setBackground(Color.WHITE);
			btnNewButton_7.setBackground(Color.WHITE);
			btnNewButton_8.setBackground(Color.WHITE);
			btnNewButton_9.setBackground(Color.WHITE);
			btnNewButton_10.setBackground(Color.WHITE);
			btnNewButton_11.setBackground(Color.WHITE);
			btnNewButton_12.setBackground(Color.WHITE);
			btnNewButton_13.setBackground(Color.WHITE);
			btnNewButton_14.setBackground(Color.WHITE);
			btnNewButton_15.setBackground(Color.WHITE);
			btnNewButton_16.setBackground(Color.WHITE);
			btnNewButton_17.setBackground(Color.WHITE);
			btnNewButton_18.setBackground(Color.WHITE);	
			btnNewButton_19.setBackground(Color.WHITE);
			btnNewButton_20.setBackground(Color.WHITE);
			btnNewButton_21.setBackground(Color.WHITE);
			btnNewButton_22.setBackground(Color.WHITE);
			btnNewButton_22.setBackground(Color.WHITE);
			btnNewButton_23.setBackground(Color.WHITE);
			btnNewButton_24.setBackground(Color.WHITE);
			btnNewButton_25.setBackground(Color.WHITE);
			btnNewButton_26.setBackground(Color.WHITE);
			btnNewButton_28.setBackground(Color.WHITE);
			btnNewButton_30.setBackground(Color.WHITE);	
	}
		/*	public void keyTyped(java.awt.event.KeyEvent e) {	
				if (car == KeyEvent.VK_W) {
					btnNewButton_1.setBackground(null);
				}
				else if(car == KeyEvent.VK_E) {
					btnNewButton_2.setBackground(null);
				}
				else if (car == KeyEvent.VK_R) {
					btnNewButton_3.setBackground(null);
				}
				else if (car == KeyEvent.VK_T) {
					btnNewButton_4.setBackground(null);
				}
				else if (car == KeyEvent.VK_Y) {
					btnNewButton_5.setBackground(null);
				}
				else if (car == KeyEvent.VK_U) {
					btnNewButton_6.setBackground(null);
				}
				else if (car == KeyEvent.VK_I) {
					btnNewButton_7.setBackground(null);
				}
				else if (car == KeyEvent.VK_O) {
					btnNewButton_8.setBackground(null);
				}
				else if (car == KeyEvent.VK_P) {
					btnNewButton_9.setBackground(null);
				}
				else if (car == KeyEvent.VK_A) {
					btnNewButton_10.setBackground(null);
				}
				else if (car == KeyEvent.VK_S) {
					btnNewButton_11.setBackground(null);
				}
				else if (car == KeyEvent.VK_D) {
					btnNewButton_12.setBackground(null);
				}
				else if (car == KeyEvent.VK_F) {
					btnNewButton_13.setBackground(null);
				}
				else if (car == KeyEvent.VK_G) {
					btnNewButton_14.setBackground(null);
				}
				else if (car == KeyEvent.VK_H) {
					btnNewButton_15.setBackground(null);
				}
				else if (car == KeyEvent.VK_J) {
					btnNewButton_16.setBackground(null);
				}
				else if (car == KeyEvent.VK_K) {
					btnNewButton_17.setBackground(null);
				}
				else if (car == KeyEvent.VK_L) {
					btnNewButton_18.setBackground(null);
				}
				else if (car == KeyEvent.VK_Q) {
					btnNewButton_19.setBackground(null);
				}
				else if (car == KeyEvent.VK_Z) {
					btnNewButton_20.setBackground(null);
				}
				else if (car == KeyEvent.VK_X) {
					btnNewButton_21.setBackground(null);
				}
				else if (car == KeyEvent.VK_C) {
					btnNewButton_22.setBackground(null);
				}
				else if (car == KeyEvent.VK_V) {
					btnNewButton_23.setBackground(null);
				}
				else if (car == KeyEvent.VK_B) {
					btnNewButton_24.setBackground(null);
				}
				else if (car == KeyEvent.VK_N) {
					btnNewButton_25.setBackground(null);
				}
				else if (car == KeyEvent.VK_M) {
					btnNewButton_26.setBackground(null);
				}
				else if (car == KeyEvent.VK_PERIOD || car == KeyEvent.VK_DECIMAL){ //punto
					btnNewButton_28.setBackground(null);
				}
				else if (car == KeyEvent.VK_SPACE){
					btnNewButton_30.setBackground(null);
				}	
			}*/
			
		});
		
	
	}

	public static char[] getArr() {
		return arr;
	}

	public static void setArr(char[] arr) {
		partefinal.arr = arr;
	}
}
    
 
