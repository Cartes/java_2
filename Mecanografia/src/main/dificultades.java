package main;
import java.awt.EventQueue;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class dificultades {

	private JFrame frmMecanografia;

	public JFrame getFrame() {
		return frmMecanografia;
	}

	public void setFrame(JFrame frame) {
		this.frmMecanografia = frame;
	}

	
	static public partefinal partefinal1 = new partefinal();

	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					dificultades window = new dificultades();
					window.frmMecanografia.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public dificultades() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmMecanografia = new JFrame();
		frmMecanografia.setIconImage(Toolkit.getDefaultToolkit().getImage("images/imagen1.png"));
		frmMecanografia.setTitle("Mecanografia");
		frmMecanografia.setBounds(100, 100, 749, 428);
		frmMecanografia.setLocationRelativeTo(null);
		frmMecanografia.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmMecanografia.getContentPane().setLayout(null);
		
		JButton botton1 = new JButton("Texto Facil");
		botton1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				partefinal1.nivel(0);
				partefinal1.getFrame().setVisible(true);
				frmMecanografia.setVisible(false);
			}
		});
		botton1.setBounds(246, 53, 242, 93);
		frmMecanografia.getContentPane().add(botton1);
		
		JButton botton2 = new JButton("Texto Dificil");
		botton2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				partefinal1.nivel(1);
				partefinal1.getFrame().setVisible(true);;
				frmMecanografia.setVisible(false);
			}
		});
		botton2.setBounds(246, 219, 242, 93);
		frmMecanografia.getContentPane().add(botton2);
		
		JButton btnNewButton = new JButton("Volver");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				login.principales.getFrame().setVisible(true);
				frmMecanografia.setVisible(false);
			}
		});
		btnNewButton.setBounds(577, 344, 130, 34);
		frmMecanografia.getContentPane().add(btnNewButton);
		
		
	}
}
