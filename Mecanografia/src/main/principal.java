package main;
import java.awt.EventQueue;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class principal {

	private JFrame frmMecanografia;
	
	public JFrame getFrame() {
		return frmMecanografia;
	}

	public void setFrame(JFrame frame) {
		this.frmMecanografia = frame;
	}

	static dificultades dificult = new dificultades();
	estadisticas stats = new estadisticas();
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					principal window = new principal();
					window.frmMecanografia.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public principal() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmMecanografia = new JFrame();
		frmMecanografia.setIconImage(Toolkit.getDefaultToolkit().getImage("images/imagen1.png"));
		frmMecanografia.setTitle("Mecanografia");
		frmMecanografia.setBounds(100, 100, 579, 391);
		frmMecanografia.setLocationRelativeTo(null);
		frmMecanografia.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmMecanografia.getContentPane().setLayout(null);
		
		JButton btnNewButton = new JButton("Lecciones");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dificult.getFrame().setVisible(true);
				frmMecanografia.setVisible(false);
			}
		});
		btnNewButton.setBounds(198, 86, 172, 39);
		frmMecanografia.getContentPane().add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Estadisticas");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				stats.getFrame().setVisible(true);
				frmMecanografia.setVisible(false);
			}
		});
		btnNewButton_1.setBounds(198, 154, 172, 39);
		frmMecanografia.getContentPane().add(btnNewButton_1);
		
		JButton btnNewButton_2 = new JButton("Volver");
		btnNewButton_2.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				
			logo.alLogin.getFrame().setVisible(true);
			frmMecanografia.setVisible(false);
			}
		});
		btnNewButton_2.setBounds(437, 318, 100, 23);
		frmMecanografia.getContentPane().add(btnNewButton_2);
	}
}
