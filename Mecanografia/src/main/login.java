package main;
import java.awt.EventQueue;
import java.awt.Toolkit;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;
import java.awt.Color;

public class login {

	private JFrame frame;
	public JFrame getFrame() {
		return frame;
	}
	
	public void setFrame(JFrame frame) {
		this.frame = frame;
	}
	

	private JTextField textField;
	private JPasswordField passwordField;
	private JButton botton1;
	
	public static String nombre;
	
	public static principal principales = new principal();
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					login window = new login();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	public void Comprueba () {
		
		String text="";
		
		nombre= textField.getText();
		String passwrd=passwordField.getText();
		
		boolean tiene = false;
		String filename = "users/usuarios.txt";
		
		File f = new File(filename);
		
			FileReader fr;
			try {
				fr = new FileReader(filename);
				BufferedReader br = new BufferedReader(fr);
	
					while ((text=br.readLine()) != null) {
					int space=text.indexOf("*");
					String nombre1=text.substring(0, space);
					String passwrd1=text.substring(space+1);
					if ((nombre1.contentEquals(nombre)) && (passwrd1.contentEquals(passwrd))) {
						principales.getFrame().setVisible(true);
						frame.setVisible(false);
						tiene = true;
						}		
					}		
					if (!tiene)
					{
						JOptionPane.showMessageDialog(null, "Usuario o contrasena incorrecto","ERROR", JOptionPane.WARNING_MESSAGE);
					}
					br.close();		
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				System.out.println("No puedes entrar");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}
	public login() {
		initialize();
	}
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(Color.GRAY);
		frame.setIconImage(Toolkit.getDefaultToolkit().getImage("images/imagen1.png"));
		frame.setTitle("Mecanografia");
		frame.setBounds(100, 100, 650, 442);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblUsuario = new JLabel("Usuario");
		lblUsuario.setBounds(208, 108, 69, 20);
		frame.getContentPane().add(lblUsuario);
		
		JLabel lblContrasea = new JLabel("Contrase\u00F1a");
		lblContrasea.setBounds(208, 161, 69, 14);
		frame.getContentPane().add(lblContrasea);
		
		textField = new JTextField();
		textField.setBounds(287, 108, 128, 20);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(287, 158, 128, 20);
		frame.getContentPane().add(passwordField);

		botton1 = new JButton("Aceptar");
		botton1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Comprueba();
			}
		});
		botton1.setBounds(326, 200, 89, 23);
		frame.getContentPane().add(botton1);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setBackground(Color.LIGHT_GRAY);
		lblNewLabel.setIcon(new ImageIcon("images\\imagen_login.jpg"));
		lblNewLabel.setBounds(0, 0, 686, 403);
		frame.getContentPane().add(lblNewLabel);
		
		JButton btnNewButton = new JButton("Salir");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		btnNewButton.setBounds(517, 369, 89, 23);
		frame.getContentPane().add(btnNewButton);
	}
}
