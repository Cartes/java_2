package main;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTextPane;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Toolkit;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.awt.event.ActionEvent;

public class estadisticas {

	private JFrame frmMecanografia;
	
	public JFrame getFrame() {
		return frmMecanografia;
	}

	public void setFrame(JFrame frame) {
		this.frmMecanografia = frame;
	}
	static JTextPane textPane = new JTextPane();
	
	public static void leerFicheroEst()
	{
		String linea;
		
		try {
			File fichero = new File("users\\estadisticas.txt");
			FileReader fr = new FileReader(fichero);
			BufferedReader bf = new BufferedReader(fr);
			
			linea = bf.readLine();
			
			while (linea != null)
			{
				textPane.setText(linea);
				linea = bf.readLine();
			}

		}catch(Exception e) {e.printStackTrace();}
		
	}


	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					estadisticas window = new estadisticas();
					window.frmMecanografia.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public estadisticas() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmMecanografia = new JFrame();
		frmMecanografia.setIconImage(Toolkit.getDefaultToolkit().getImage("images/imagen1.png"));
		frmMecanografia.setTitle("Mecanografia");
		frmMecanografia.setBounds(100, 100, 613, 427);
		frmMecanografia.setLocationRelativeTo(null);
		frmMecanografia.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmMecanografia.getContentPane().setLayout(null);
		
		JTextPane textPane = new JTextPane();
		textPane.setBounds(23, 97, 382, 253);
		frmMecanografia.getContentPane().add(textPane);
		
		JLabel lblNewLabel = new JLabel("Estadisticas de los Usuarios");
		lblNewLabel.setFont(new Font("Yu Gothic UI Semibold", Font.PLAIN, 16));
		lblNewLabel.setBounds(23, 63, 278, 23);
		frmMecanografia.getContentPane().add(lblNewLabel);
		
		JButton btnNewButton = new JButton("Volver");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				login.principales.getFrame().setVisible(true);
				frmMecanografia.setVisible(false);
			}
		});
		btnNewButton.setBounds(453, 354, 123, 23);
		frmMecanografia.getContentPane().add(btnNewButton);
		
		leerFicheroEst();
	}
}
