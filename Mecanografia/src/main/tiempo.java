package main;


import java.util.Timer;
import java.util.TimerTask;

public class tiempo {
	

	 private Timer timer = new Timer(); 
	    private static int segundos=1;

	    //Clase interna que funciona como contador
	    class Contador extends TimerTask {
	        public void run() {
	            segundos++;
	            //System.out.println("segundo: " + segundos);
	            partefinal.run();
	        }
	    }
	    //Crea un timer, inicia segundos a 0 y comienza a contar
	    public void Contar()
	    {
	        this.segundos=0;
	        timer = new Timer();
	        timer.scheduleAtFixedRate(new Contador(), 1000, 1000);
	    }
	    //Detiene el contador
	    public void Detener() {
	        timer.cancel();
	    }
	    //Metodo que retorna los segundos transcurridos
	    public int getSegundos()
	    {
	        return segundos;
	    }


}
