package main;
import java.awt.EventQueue;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JProgressBar;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;

public class logo {

	private JFrame frame;
	
	public static login alLogin = new login();
	
	public static File contraseņa = new File("users/usuarios.txt");
	public static File textos = new File("users/archivo.txt");
	public static File estadisticas = new File("users/estadisticas.txt");

	/**1
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					logo window = new logo();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	public static void compruebFichero(File nombre)
	{
		if (nombre.exists())
		{
			System.out.println("Todos los ficheros estan correctamente");
		}
		else 
		{
			JOptionPane.showMessageDialog(null, "No se han cargado bien los ficheros. ERROR 2","ERROR", JOptionPane.WARNING_MESSAGE);
			System.exit(0);
		}
	}
	
	public static void ficherosComprobados() {
		compruebFichero(contraseņa);
		compruebFichero(textos);
		compruebFichero(estadisticas);
	}
	

	/**
	 * Create the application.
	 */
	public logo() {
		initialize();
	}

	
	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setIconImage(Toolkit.getDefaultToolkit().getImage("images/imagen1.png"));
		frame.setTitle("Mecanografia");
		frame.setBounds(100, 100, 651, 491);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JProgressBar progressBar = new JProgressBar();
		progressBar.setBounds(251, 324, 184, 23);
		progressBar.setIndeterminate(true);
		frame.getContentPane().add(progressBar);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon("images/iamsen.jpg"));
		lblNewLabel.setBounds(0, 0, 635, 452);
		frame.getContentPane().add(lblNewLabel);
		
		
		JButton btnNewButton = new JButton("Iniciar");
		btnNewButton.setBounds(294, 373, 89, 23);
		frame.getContentPane().add(btnNewButton);
		
		btnNewButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				
				ficherosComprobados();
				alLogin.getFrame().setVisible(true);
				frame.setVisible(false);
			}
		});
	}
	
	
}
