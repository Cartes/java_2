import java.util.Scanner;
import java.util.InputMismatchException;

public class Principal4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		 int n=0;
		 int estacionamientoCoche = 0;
		 int estacionamientoMoto = 0;
		 int estacionamientoCamion= 0;
		 int resultado=0;
		 // int recaudacion=0;
		 
		 do {
			 System.out.println("--------------------MENU---------------");
			 System.out.println("---------------------------------------");
			 System.out.println("1. Aparcar Coche(5€)");
			 System.out.println("2. Aparcar Moto(2€)");
			 System.out.println("3. Aparcar Camion(8€)");
			 System.out.println("4. Ver recaudarcion");
			 System.out.println("5. Salir");
			 System.out.println("Escriba la opcion: ");
			 
			//Crea objeto teclado de la clase Scanner		 
			Scanner teclado = new Scanner(System.in);
			//Es un escudo
			try {
			n = teclado.nextInt();
			} catch (InputMismatchException error) { //Hemos capturado el error con el catch
				System.out.println("Introduzca la opcion de forma correcta");
			}
			
			switch(n) {
			case 1:
				System.out.println("Aparcando Coche");
				System.out.print("Digame la matricula de su coche: ");
				String matricula = teclado.next();
				estacionamientoCoche++;
				//recaudacion = recaudacion + 5;
				break;
			
			case 2:
				System.out.println("Aparcando Moto..");
				System.out.println("Dime la matricula: ");
				String matricula1 = teclado.next();
				estacionamientoMoto++;
				break;
				
			case 3:
				System.out.println("Aparcando Camion");
				System.out.println("Dime la matricula: ");
				String matricula2 = teclado.next();
				estacionamientoCamion++;
				break;
				
			case 4:
				System.out.println("Ver recaudacion: ");
				resultado = (estacionamientoCoche*5)+(estacionamientoMoto*2)+ (estacionamientoCamion*8);
				System.out.println("La total de recaudacion es:"+resultado/*+recaudacion*/);
				break;
			
			case 5:
				System.out.println("Hasta luego");
				break;
			default:
				System.out.println("Eleccion no valida");
				break;
				
			
	
			} 
		 } while(n != 5 );

	}
	
	

}
