import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Principal {

	private JFrame frmPir;
	public JFrame getFrmPir() {
		return frmPir;
	}

	public void setFrmPir(JFrame frmPir) {
		this.frmPir = frmPir;
	}

	public hija getHija() {
		return hija;
	}

	public void setHija(hija hija) {
		this.hija = hija;
	}

	hija hija = new hija(this);

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Principal window = new Principal();
					window.frmPir.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Principal() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmPir = new JFrame();
		frmPir.setTitle("PRINCIPAL");
		frmPir.setBounds(100, 100, 595, 414);
		frmPir.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmPir.getContentPane().setLayout(null);
		
		JButton btnNewButton = new JButton("IR A HIJA -->");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnNewButton.setBounds(139, 75, 295, 63);
		frmPir.getContentPane().add(btnNewButton);
		
		//Eventos
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				hija.getFrmHija().setVisible(true);
				frmPir.setVisible(false);
			}
		});
	}
}
