import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class hija {

	private JFrame frmHija;
	public JFrame getFrmHija() {
		return frmHija;
	}


	public Principal getPadre() {
		return padre;
	}

	public void setPadre(Principal padre) {
		this.padre = padre;
	}

	public void setFrmHija(JFrame frmHija) {
		this.frmHija = frmHija;
	}


	Principal padre;

	public hija(Principal padre) {
		initialize();
		this.padre = padre;
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmHija = new JFrame();
		frmHija.setTitle("HIJA");
		frmHija.setBounds(100, 100, 636, 433);
		frmHija.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmHija.getContentPane().setLayout(null);
		
		JButton btnNewButton = new JButton("<-- IR AL PADRE ");
		
		btnNewButton.setBounds(184, 124, 285, 76);
		frmHija.getContentPane().add(btnNewButton);
		
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				padre.getFrmPir().setVisible(true);
				frmHija.setVisible(false);
			}
		});
	}

}
