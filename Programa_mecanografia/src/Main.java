import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Toolkit;
import java.awt.FlowLayout;
import java.awt.Color;
import javax.swing.BoxLayout;
import javax.swing.JTextPane;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class Main extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Main frame = new Main();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Main() {
		setIconImage(Toolkit.getDefaultToolkit().getImage("C:\\Users\\DAM\\Desktop\\Mercancia_Odoo\\arbeloa.PNG"));
		setTitle("Mecanografia");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 867, 513);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(139, 69, 19));
		contentPane.setForeground(Color.BLACK);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JTextPane texto = new JTextPane();
		texto.setBounds(10, 11, 831, 181);
		contentPane.add(texto);
		
		JButton btnNewButton = new JButton("boton");
		btnNewButton.setBounds(56, 249, 89, 23);
		contentPane.add(btnNewButton);
		
		JButton A = new JButton("A");
		A.setBounds(187, 249, 89, 23);
		contentPane.add(A);
	
	btnNewButton.addActionListener(new ActionListener() {
		
		
		public void actionPerformed(ActionEvent arg0) {
			texto.setEnabled(false);
		}
	});
	
	A.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			texto.setText(texto.getText() + "A");
		}
	});
	
}
}
