import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.InputMismatchException;
import java.util.Scanner;

public class main {
	
	static Scanner teclado = new Scanner(System.in);
	
	public static void menu() {
		System.out.println("\nMenu\n");
		System.out.println("1.Escribir datos para guardalo en un fichero");
		System.out.println("2.Abrir un fichero con 10 numeros y guardarlos en una tabla");
		System.out.println("3.Escribir por la consola los datos de la tabla");
		System.out.println("4.indicar la posicion quese quiere modificar de la tabla.");
		System.out.println("5. guardar los datos existentes en la tabla sobre un fichero");
		System.out.println("6.Salir");
		
	}
	/*
	public static void leerFichero(int [] nombres) {
		
		try {
			
			File fichero = new File("numeros.txt");
			FileReader fr= new FileReader(fichero);
			
			//2.Lectura del Fichero
			int letra;
			String cadena = "";
			int posicion = 0;
			
			letra = fr.read();
			while (letra != -1) {
				
				if (letra==' ') {
					nombres[posicion] = cadena;
					cadena="";
					posicion++;
				}
				else
				cadena += ((char)letra); //pone los caracteres
				
				//Leer el siguiente caracter
				letra = fr.read();
			}
			//3.Cerrar el fichero
			fr.close();
			
			//4.Escribir la cadena por pantalla
			
			System.out.println("He leido: "+cadena);
		} catch (FileNotFoundException e) {System.out.println(e.getMessage());} catch (IOException e){System.out.println(e.getMessage());}
	}
*/
	public static void escribirFichero(String nombre, int[] tabla) {

		try {
			File archivo = new File (nombre);
			FileWriter fw = new FileWriter(archivo);

			for (int i=0; i<tabla.length; i++) 
			{
				fw.write(tabla[i] + " ");
			}
			fw.close();
		} catch (IOException e) {System.out.println(e.getMessage());}
		
	}
	
	public static void escribirFicheroV3_SALTO(String nombre, int[] tabla) {

		try {
			File archivo = new File (nombre);
			FileWriter fw = new FileWriter(archivo);

			for (int i=0; i<tabla.length; i++) 
			{
				fw.write(tabla[i] + "\n");
			}
			fw.close();
		} catch (IOException e) {System.out.println(e.getMessage());}
		
	}
	
	public static void LeerTablaFichero(String nombre, int[] tabla)
	{
		int resultado,numeroEntero = 0,i;
		String numeroCadena;
		try {
			
			File fichero = new File(nombre);
			FileReader fr= new FileReader(fichero);
	
		i=0;
		numeroCadena="";
		resultado = fr.read();
		while (resultado != -1) //leemos caracter a caracter
		{
			if (resultado == ' ')// guarda los blancos
			{
				numeroEntero = Integer.parseInt(numeroCadena);
				tabla[i] = numeroEntero;
				i++; // incrementa en el array
				numeroCadena = "";
			}
			else
				numeroCadena += (char)resultado; //va metiendo los caracteres a la cadena
			
			resultado = fr.read();
		}
		fr.close();
			for(int i1=0; i1<10; i1++){
				System.out.println(tabla[i1]+"");
				System.out.println("");
				
				
			}
		} catch (IOException e) {System.out.println(e.getMessage());}

	}
	
	public static void LeerTablaFicheroV2(String nombre, int[] tabla)
	{
		int resultado,numeroEntero,i;
		try {
			
			File fichero = new File(nombre);
			FileReader fr= new FileReader(fichero);
	
		i=0;

		numeroEntero=0;
		resultado = fr.read();
		while (resultado != -1) //leemos caracter a caracter
		{
			if (resultado == ' ')// guarda los blancos
			{
				tabla[i] = numeroEntero;
				i++; // incrementa en el array
				numeroEntero = 0;
			}
			else
				numeroEntero = (numeroEntero*10) + (resultado-'0');
			
			resultado = fr.read();
		}
		fr.close();
			for(int i1=0; i1<10; i1++){
				System.out.println(tabla[i1]+"");
				System.out.println("");
				
				
			}
		} catch (IOException e) {System.out.println(e.getMessage());}

	}
	
	public static void LeerTablaFicheroV3_con_salto(String nombre, int[] tabla)
	{
		int resultado,numeroEntero = 0,i;
		String numeroCadena;
		try {
			
			File fichero = new File(nombre);
			FileReader fr= new FileReader(fichero);
	
		i=0;
		numeroCadena="";
		resultado = fr.read();
		while (resultado != -1) //leemos caracter a caracter
		{
			if ((resultado == '\r') || (resultado == 'n'))// guarda los blancos
			{
				numeroEntero = Integer.parseInt(numeroCadena);
				tabla[i] = numeroEntero;
				i++; // incrementa en el array
				numeroCadena = "";
				resultado = fr.read();
				if (resultado == 'r')
					resultado = fr.read();
			}
			else
				numeroCadena += (char)resultado; //va metiendo los caracteres a la cadena
			resultado = fr.read();
		}
		fr.close();
			for(int i1=0; i1<10; i1++){
				System.out.println(tabla[i1]+"");
				System.out.println("");
				
				
			}
		} catch (IOException e) {System.out.println(e.getMessage());}

	}
	
	public static void escribir (String nombre) {
		System.out.println(nombre);
	}
	
	public static void modificar(int[] nombr) {
		
		int pos,numeroNuevo;
		
		Scanner teclado = new Scanner (System.in);
		
		System.out.println("Que posición quieres cambiar: ");
		pos = teclado.nextInt();
		System.out.println("Que valor quieres añadir en la posicion indicada: ");
		numeroNuevo = teclado.nextInt();
		
		nombr[pos] = numeroNuevo;

	}
	
	

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		
		int opcion = 0;
		
		int [] nombr = {1,2,3,4,5,6,7,8,9,10};
	
		do {
			
			menu();
			
			try {
			opcion = teclado.nextInt();	
			} catch (InputMismatchException error) { //Hemos capturado el error con el catch
				System.out.println("Introduzca la opcion de forma correcta");
			}
			switch(opcion) {
			
			case 1:
				escribirFicheroV3_SALTO("numeros.txt",nombr);
				break;
				
			case 2:

				LeerTablaFicheroV3_con_salto("numeros.txt",nombr);
				break;
			case 3:
				escribir("numeros.txt");
				break;
			case 4:
				//Modificacion de tablanombr[posicion] = nuevaposicion;
			modificar(nombr);
			break;
			
			case 5:
				LeerTablaFichero("numeros.txt",nombr);
				
			case 6:
				System.out.println("adios");
				break;
			}
		}while (opcion != 6);
		

		
	}
}





/*
1. Crear un programa que cree un fichero de texto con una lista de 10 números enteros. A continuación,
mostrará un menú con:
1. Escribir fichero: escribir los datos contenidos en una tabla y guardarlos en un fichero.
2. Leer fichero: abrir un fichero con 10 números y guardarlos en una tabla.
3. Escribir: escribir por la consola los datos de la tabla.
4. Modificar tabla: Indicar la posición que se quiere modificar de la tabla y modificar los datos.
5. Actualizar tabla fichero: Guardar los datos existentes en la tabla sobre el fichero asociado.
6. Salir.
Nota:
 La escritura y lectura se realizará sobre FileWriter y FileReader.
2. Crear un programa en Java que escriba una lista de nombres existentes en un tabla de Strings. Se
realizarán dos métodos:
1. escribirFichero: Recibe como parámetro una tabla de Strings rellena con nombres y escribe
sobre un fichero de texto el contenido de la tabla.
2. leerFichero: Recibe como parámetro una tabla de Strings vacía y lee de un fichero los datos
existentes en el mismo y los almacena en la tabla pasado como parámetro.
Nota:
 La escritura y lectura se realizará sobre FileWriter y FileReader
*/