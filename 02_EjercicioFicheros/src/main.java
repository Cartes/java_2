import java.io.File;
import java.io.IOException;

public class main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		
		String byc = "bin\\bytecode";
		String obj = "bin\\objetos";
		
		File bin_Subcarpeta1 = new File(byc);
		File bin_Subcarpeta2 = new File(obj);
		
		
		if (bin_Subcarpeta1.mkdirs()) 
		{
			if(bin_Subcarpeta2.mkdirs()) 
			{
			System.out.println("He creado las carpetas en BIN");
			}
		}
		else 
		{
			System.out.println("No puedo");
		}
		
		String cls = "src\\clases";
		File src_SubCarpeta1 = new File(cls);
		
		if (src_SubCarpeta1.mkdirs()) {
			System.out.println("He creado la Subcarpeta dentro de SRC");
			
			File archivo1 = new File (src_SubCarpeta1, "uno.java");
			
			try {
				archivo1.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				System.out.println("No he podido crear el fichero");
			}
		}
		else 
		{
			System.out.println("No he podido crear la subcarpeta en SRC");
		}
		
		File doc = new File("doc"); doc.mkdir();
		String html = "doc\\html";
		String pdf =  "doc\\pdf";

		
		File doc_subCarpeta1 = new File(html);
		File doc_subCarpeta2 = new File(pdf);
		
		if (doc_subCarpeta1.mkdirs()) {
			File archivoHTML = new File(doc_subCarpeta1, "readnoHTML");
			
			try {
				archivoHTML.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				System.out.println("No he podido crear el fichero");
			}
			if(doc_subCarpeta2.mkdirs()) 
			{
			System.out.println("He creado las carpetas en DOC");
			}
			
		}
		else {
			System.out.println("No puedo");
		}
		
		
		
/*
		String [] ruta = {"bin\\bytecode", "bin\\objetos"};
		
		for(int i=0; i<ruta.length; i++) {
		File rutaL = new File(ruta[i]);
		}
		
		if (rutaL.mkdirs()) {
			System.out.println("He creado el fichero");
		}
		else
		{
			System.out.println("No lo he creado");
		}
*/		
	}

}
